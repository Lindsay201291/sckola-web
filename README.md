# Sckola web

This project was generated with [Angular JS] version 1.5+.

## Getting started

 1.- clone repo
 2.- bower install

## Configure Server

 To configure the servers pointed to by sckolaReporte, go to app.js and modify the domainServiceUrl and domainReportUrl.
 
 domainServiceUrl to sckola server.
 
 domainReportUrl to sckolaReporte.
