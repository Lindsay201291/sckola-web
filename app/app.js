(function(){
	'use strict';
	angular.module('sckola',
		['ngResource',
			'ngAnimate',
			'ngWebSocket',
			'toastr',
			'angular-notification-icons',
			'angular-desktop-notification',
			'ui.router',
			'slickCarousel',
			'ui.bootstrap', 'ui.bootstrap.tpls',
			'matchmedia-ng',
			'sckola.login',
			'sckola.dashboard',
			'sckola.termsOfUse',
			'sckola.user',
			'sckola.community',
			'sckola.matter',
			'sckola.assistance',
			'sckola.students',
			'sckola.evaluationPlan',
			'sckola.uploadNotes'])
		.config(function($stateProvider, $urlRouterProvider,$rootScopeProvider){
			/*$urlRouterProvider.otherwise('/');
			 $stateProvider
			 .state('root',{
			 url:'/',
			 views: {
			 navbar: {
			 templateUrl: 'modules/navbar/partials/navbar.html',
			 controller: 'NavBarCtrl',
			 controllerAs: 'vm'
			 },
			 sidemenu:{
			 templateUrl: 'modules/sidemenu/partials/sidemenu.html',
			 controller: 'SideMenuCtrl',
			 controllerAs: 'vm'
			 },
			 content:{
			 templateUrl: 'modules/feed/partials/feed.html',
			 controller: 'FeedCtrl',
			 controllerAs: 'vm'
			 }
			 }
			 });*/
			$urlRouterProvider.otherwise('/');
			$rootScopeProvider.constructor();
			$stateProvider
				.state('index', {
					abstract: true,
					views: {
						/*'navbar@': {
						 templateUrl: 'modules/bar/partials/navbar.html',
						 controller: 'NavbarCtrl',
						 controllerAs: 'vm'
						 },
						 'footer@': {
						 templateUrl: 'modules/bar/partials/footer.html',
						 controller: 'NavbarCtrl',
						 controllerAs: 'vm'
						 },
						 'profile@': {
						 templateUrl: "modules/user/partials/profile.html",
						 controller: "ProfileCtrl",
						 controllerAs: 'vm'
						 },
						 /*'news@': {
						 templateUrl: "modules/news/partials/news.html",
						 controller: "NewsCtrl",
						 controllerAs: 'vm'
						 }*/
					}
				})
				.state('root', {
					url: '/',
					views: {
						'navbar@': {
							templateUrl: "modules/login/partials/login.html",
							controller: "LoginCtrl",
							controllerAs: "vm"
						},
						/*'banner@': {
						 templateUrl: "modules/home/partials/banner.html",
						 controller: "HomeCtrl",
						 controllerAs: "vm"
						 },*/
						'home@': {
							templateUrl: "modules/home/partials/home.html",
							controller: "HomeCtrl",
							controllerAs: "vm"
						}
					}
				})
				.state('validateUserCode', {
					url: '/user/validate?code',
					views: {
						'navbar@': {
							templateUrl: "modules/login/partials/login.html",
							controller: "LoginCtrl",
							controllerAs: "vm"
						},
						'banner@': {
							templateUrl: "modules/home/partials/banner.html",
							controller: "HomeCtrl",
							controllerAs: "vm"
						},
						'home@': {
							templateUrl: "modules/home/partials/home.html",
							controller: "HomeCtrl",
							controllerAs: "vm"
						}
					}

				})
		})
		.run(function ($rootScope) {
                	$rootScope.domainServiceUrl = 'http://api.sckola.com/';
					$rootScope.domainReportUrl = 'http://192.241.186.161:8070/';
					$rootScope.domainUrlHelp = './static/pdf/MU-SCKOLA-V1.4.pdf';

			$rootScope.justNumbers = function($event){
				if($event !== undefined){
					if(String.fromCharCode($event.keyCode) !== "."){
						if(isNaN(String.fromCharCode($event.keyCode))){
							$event.preventDefault();
						}
					}
					var p = 0;
					for(var i = 0;i < $event.target.value.length;i++){
						if($event.target.value.charAt(i) == "."){
							p++;
						}
						if((p === 1) && String.fromCharCode($event.keyCode) === "."){
							$event.preventDefault();
						}
					}
				}
			};
		});
})();
