(function(){
    'use strict';

    angular
        .module('sckola')
        .controller('HomeCtrl', HomeCtrl);

    HomeCtrl.$inject = ['$scope', '$uibModal', '$state','ContactMe'];
    function HomeCtrl ($scope, $uibModal, $state, ContactMe){
        var vm = this;
        vm.contact = {'name': null,'email':null,'message':null};
        vm.message = "mensaje inicial HOLA MUNDO :)";
        vm.acceptButton = false;
        vm.cancelButton = false;
        vm.okButton = false;
        vm.okGo = false;
        vm.checkButton = false;

        //Configuracion con la applicacion de facebook Connect
        /*window.fbAsyncInit = function() {
            FB.init({
                appId            : '1782384328492412',
                autoLogAppEvents : true,
                xfbml            : true,
                version          : 'v3.1'
            });
        };
        //SDK DE FACEBOOK
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = 'https://connect.facebook.net/es_LA/sdk.js';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));*/

        vm.matter = function(){
            //alert ('antes de stateGo');
            $state.go('matterHome');
        };

        vm.terms = function(){
            $state.go('termsOfUse');
        };

        vm.contactMe = function(){
            if(vm.contact.name != null && vm.contact.name != '' && vm.contact.email != null && vm.contact.email != '' && vm.contact.message != '' && vm.contact.message != null){
            vm.title = "Contáctarme";
            vm.message = 'Enviando mensaje en progreso...';
            vm.message2 = "";
            vm.acceptButton = false;
            vm.cancelButton = false;
            vm.okButton = false;
            vm.openModalMessage();
            ContactMe.save({},vm.contact,
                function(success){
                    vm.message = "Se envio satisfactoriamente el mensaje";
                    vm.okButton = true;
                    vm.contact = {'name': null,'email':null,'message':null};
                }, function (error) {
                    vm.message = "Disculpe hubo un error en el envio del mensaje, Intentelo mas tarde";
                    vm.okButton = true;
                });
            }
        };

        vm.openModalMessage = function(){
            $scope.modalInstance = $uibModal.open({
                templateUrl: 'template/modal/modal_message.html',
                scope: $scope,
                windowClass: 'app-modal-window',
                size: 'sm',
                keyboard  : false
            });
        };

        vm.cancel = function () {
            $scope.modalInstance.close('close');
        };

        vm.close = function () {
            if($scope.modalInstance !== undefined)
                $scope.modalInstance.close('close');
            if($scope.modalInstanceRegistro !== undefined)
                $scope.modalInstanceRegistro.close('close')
        };

        vm.ok = function () {
            if($scope.modalInstance !== undefined)
                $scope.modalInstance.close('close');
            if($scope.modalInstanceRegistro !== undefined)
                $scope.modalInstanceRegistro.close('close')
        };
    }
})();
