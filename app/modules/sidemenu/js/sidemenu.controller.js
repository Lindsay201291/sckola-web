(function(){
  'use strict';
  angular.module('sckola')
         .controller('SideMenuCtrl',SideMenuCtrl);

  SideMenuCtrl.$inject = ['$scope','$state','authentication', '$rootScope', 'UserUpdate', 'UserPhoto', '$uibModal'];
  function SideMenuCtrl($scope,$state,authentication, $rootScope, UserUpdate, UserPhoto, $uibModal){
    var vm = this;
    vm.urlToHelp = $rootScope.domainUrlHelp;
    vm.urlToApk = $rootScope.domainUrlApk;
      vm.user = authentication.getUser();
      vm.role = vm.user.roleList[0].name;
      vm.roleName;
      if (vm.role == "TEACHER") {
        vm.roleName = "PROFESOR";
      } else if (vm.role == "STUDENT") {
        vm.roleName = "ESTUDIANTE";
      } else if (vm.role == "REPRESENTATIVE") {
        vm.roleName = "REPRESENTANTE";
      }

    vm.goToCommunities = function(){
      $state.go('communities');
    };

    vm.goToCourses = function(){
      $state.go('matterHome');
    };

    vm.goToAssistances = function(){
      $state.go("assistances");
    };

    vm.goToStudents = function(){
      $state.go("students");
    };

    vm.goToNotesStudents = function(){
        $state.go("uploadNotesStudent");
    };

      vm.goToCoursesStudents = function(){
          $state.go('matterStudent');
      };

    vm.gotToEvaluations = function(){
      $state.go("evaluationPlan");
    };

    vm.goToUploadNotes = function(){
      $state.go("uploadNotes");
    };

    /**
    * @name goToProfile
    * @desc Changes state to profile.
    * @memberOf Controllers.SideMenuController
    */
    vm.goToProfile = function(){
      $state.go("editProfile");
    };

      $scope.$on('community', function (evt, community) {
          vm.community = community;
          //vm.urlToReports = $rootScope.domainReportUrl+"Reports/$"+vm.user.id+"$"+vm.community.communityOrigin.id;
          vm.urlToReports = $rootScope.domainReportUrl+"?user="+vm.user.id+"&community="+vm.community.communityOrigin.id;
          //vm.urlToReports = $rootScope.domainReportUrl+"app/#/noAttendance?user="+vm.user.id+"&community="+vm.community.communityOrigin.id;
      });
      vm.showMyCommunities = false;

        vm.user = authentication.getUser();
        vm.community = authentication.getCommunity();
        var permission = authentication.currentUser();
        var idUser = vm.user.id;
        if(vm.community != null){
        //vm.urlToReports = $rootScope.domainReportUrl+"Reports/$"+vm.user.id+"$"+vm.community.communityOrigin.id;
        vm.urlToReports = $rootScope.domainReportUrl+"?user="+vm.user.id+"&community="+vm.community.communityOrigin.id;
        //vm.urlToReports = $rootScope.domainReportUrl+"app/#/noAttendance?user="+vm.user.id+"&community="+vm.community.communityOrigin.id;
        }
        $rootScope.tokenLoging = authentication.isLoggedIn();

        vm.loadInitial = function(){
            UserUpdate.get({userId: idUser},
                function(success){
                    vm.usuario=success.response;

                    if(vm.usuario.firstName == null || vm.usuario.firstName == ""){
                          vm.usuario.firstName = "Configurar en";
                          vm.usuario.lastName = "Perfil ";
                    }

                    if(vm.usuario.foto == null){
                          vm.usuario.foto = "static/images/usuario.png";
                    }else if (!vm.usuario.foto.includes('https')){
                        vm.usuario.foto = "static/images/" + vm.usuario.foto;
                    }
                    for(var i=0;i<vm.usuario.curriculum.length;i++){
                        if(vm.usuario.curriculum[i].date != null){
                            var formatoDate = vm.usuario.curriculum[i].date.split("-");
                            vm.usuario.curriculum[i].anio = Number(formatoDate[2]);
                        }
                    }
                    if(vm.usuario.birthdate != null){
                        var formato = vm.usuario.birthdate.split("-");
                        vm.usuario.birthdateOld = new Date(formato[2],formato[1]-1,formato[0]);
                    }
                },
                function(error){

                });
        };

        //Valida si el usuario esa autenticado con los permisos para la vista
        if (permission === null || authentication.getUser() === null) {
            $state.go('root');
            console.log("no esta autenticado:" + permission + " JSON:" + permission!==undefined?JSON.stringify(permission):"null");
        }else{
            // carga inicial por defecto
            vm.loadInitial();
        }


      $scope.$on('userUpdate', function (evt, user) {
          vm.usuario.foto = "static/images/" + user.foto;
      });


        $scope.foto = function(imagen){
           vm.imagen = new FormData();
            vm.imagen.append("file", imagen[0]);
            UserPhoto.save({userId:idUser},vm.imagen,
            function(success){
                UserUpdate.update({userId:idUser},success.response,
                function(success){
                    vm.usuario=success.response;
                    $scope.$parent.$broadcast('userSave',success.response);
                    if(vm.usuario.birthdate != null){
                        var formato = vm.usuario.birthdate.split("-");
                        vm.usuario.birthdateOld = new Date(formato[2],formato[1]-1,formato[0]);
                    }
                    $scope.$parent.$broadcast('userUpdate',success.response);
                });
            },
            function(error){
            });
       };

       vm.cerrar = function(){
        $scope.modalInstance.dismiss('cancel');
       };

       vm.showMessage = function(){
        vm.messageToUser = "Usted aún no se ha unido a ninguna comunidad";
        $scope.modalInstance = $uibModal.open({
          templateUrl: 'modules/modals/partials/modal-show-message.html',
          scope: $scope,
          size: 'sm',
          keyboard  : false
        });
      };

  }


})();
