/**
 * Controllers
 * @namespace Controllers
 */

(function(){
    'use strict';

    angular.module('sckola')
        .controller('NewsCtrl', NewsCtrl);

    NewsCtrl.$inject = ['authentication', '$rootScope', '$scope', 'NewsWizard'];
    function NewsCtrl(authentication, $rootScope, $scope, NewsWizard){
        var vm = this;

        vm.user = authentication.getUser();
        var permission = authentication.currentUser();
        var idUser = vm.user.id;
        var progress = 0;
        var sumProgress = 0;

        var bar = new ProgressBar.Circle("#circle", {
            //color: '#FFFFFF',
            // This has to be the same size as the maximum width to
            // prevent clipping
            strokeWidth: 5,
            trailWidth: 1,
            easing: 'easeInOut',
            duration: 1400,
            text: {
                autoStyleContainer: false
            },
            from: { color: '#75ABDD', width: 5 },
            to: { color: '#75ABDD', width: 5 },
            // Set default step function for all animate calls
            step: function(state, circle) {
                circle.path.setAttribute('stroke', state.color);
                circle.path.setAttribute('stroke-width', state.width);

                var value = Math.round(circle.value() * 100);

                circle.setText(value);
            }
        });

        bar.text.style.fontFamily = '"Roboto", Helvetica, Arial, sans-serif';
        bar.text.style.fontSize = '2rem';
        //Para modificar el progreso hay que configurarlo entre 0.0 y 1.0
      //  bar.animate(1.0);

//        $scope.$on('ProgressBar', function (evt, progressBar) {
  //            progress = progressBar;
    //          bar.animate(progress);
      //        vm.loadInitial();
        //    });

    NewsWizard.get({userId: idUser},
                function(success){

                    vm.usuario = success.response;

                    if(vm.usuario.profileUser != null){
                        sumProgress = sumProgress + 0.25;
                    }
                    if(vm.usuario.community != null){
                        sumProgress = sumProgress + 0.25;
                    }
                    if(vm.usuario.evaluationPlan != null){
                        sumProgress = sumProgress + 0.25;
                    }
                    if(vm.usuario.matterCommunitySection != null){
                        sumProgress = sumProgress + 0.25;
                    }

                    bar.animate(sumProgress);

                },
                function(error){

                });

    }
})();