(function(){
    'use strict';

    angular
        .module('sckola')
        .factory('NewsWizard', newsWizard);
        //.factory('ComunidadWizard', comunidadWizard)
        //.factory('Matters', matters)
        //.factory('Students',students);


    newsWizard.$inject = ['$resource','$rootScope'];
    function newsWizard($resource,$rootScope){
        return $resource($rootScope.domainServiceUrl+'/skola/wizard/user/:userId',null,
            {
                save: {
                    method: 'POST'
                },
                get: {
                    method: 'GET'
                } ,
                query: {
                    method: 'GET',
                    isArray:true
                },
                update: {
                    method: 'PUT'
                }
            });
    };


})();
