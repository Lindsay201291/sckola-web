(function(){
    'use strict';

    angular
        .module('sckola.login')
        .factory('Login', Login)
        .factory('LoginFacebook', LoginFacebook)
        .factory('RecoverPassword', recoverPassword)
        .factory('CreateUserFacebook', CreateUserFacebook)
        .factory('FacebookService', facebookService)
        .factory('SendInvitation', sendInvitation)
        .factory('LinkFacebook', linkFacebook)
        .factory('ContactMe', ContactMe);

    Login.$inject = ['$resource','$rootScope'];
    function Login($resource, $rootScope){
        return $resource($rootScope.domainServiceUrl+'/skola/login');
    };

    LoginFacebook.$inject = ['$resource','$rootScope'];
    function LoginFacebook($resource, $rootScope){
        return $resource($rootScope.domainServiceUrl+'/skola/login/facebook',null,{
            save: {
                method: 'POST'
            },
            get: {
                method: 'GET'
            } ,
            query: {
                method: 'GET',
                isArray:true
            }
        });
    };

    recoverPassword.$inject = ['$resource','$rootScope'];
    function recoverPassword($resource,$rootScope){
        return $resource($rootScope.domainServiceUrl + '/skola/user/recover',{ mail: '@mail' });
    };

    CreateUserFacebook.$inject = ['$resource','$rootScope'];
    function CreateUserFacebook($resource, $rootScope){
        return $resource($rootScope.domainServiceUrl+'/skola/user/facebook',null,
            {
                save: {
                    method: 'POST'
                    /*headers: {
                     Authorization: 'Bearer '+ authentication.getToken()
                     }*/
                },
                get: {
                    method: 'GET'
                } ,
                query: {
                    method: 'GET',
                    isArray:true
                }
            });
    };

    sendInvitation.$inject = ['$resource','$rootScope'];
    function sendInvitation($resource, $rootScope){
        return $resource($rootScope.domainServiceUrl+'/skola/invitation',null,
            {
                save: {
                    method: 'POST'
                    /*headers: {
                     Authorization: 'Bearer '+ authentication.getToken()
                     }*/
                },
                get: {
                    method: 'GET'
                } ,
                query: {
                    method: 'GET',
                    isArray:true
                }
            });
    };

    linkFacebook.$inject = ['$resource','$rootScope'];
    function linkFacebook($resource, $rootScope){
        return $resource($rootScope.domainServiceUrl+'/skola/user/facebook/update',null,
            {
                save: {
                    method: 'POST'
                    /*headers: {
                     Authorization: 'Bearer '+ authentication.getToken()
                     }*/
                },
                get: {
                    method: 'GET'
                } ,
                query: {
                    method: 'GET',
                    isArray:true
                }
            });
    };

    facebookService.$inject = [];
    function facebookService($q) {
        return {
            getMyLastName: function() {
                var deferred = $q.defer();
                FB.api('/me', {
                    fields: 'last_name'
                }, function(response) {
                    if (!response || response.error) {
                        deferred.reject('Error occured');
                    } else {
                        deferred.resolve(response);
                    }
                });
                return deferred.promise;
            }
        }
    };

    ContactMe.$inject = ['$resource','$rootScope'];
    function ContactMe($resource, $rootScope){
        return $resource($rootScope.domainServiceUrl+'/skola/contactme',null,{
            save: {
                method: 'POST'
            }
        });
    };



})();
