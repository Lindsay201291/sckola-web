(function(){
    'use strict';

    angular
        .module('sckola.login')
        .controller('LoginCtrl', LoginCtrl);

    LoginCtrl.$inject = ['$location','$scope', '$uibModal', '$state', '$rootScope', 'authentication', 'Login', 'UserRegistration', 'ValidateUserRegistration', 'RecoverPassword', 'FacebookService', 'CreateUserFacebook', 'LoginFacebook', '$timeout', 'SendInvitation', 'LinkFacebook'];
    function LoginCtrl ($location, $scope, $uibModal, $state, $rootScope, authentication, Login, UserRegistration, ValidateUserRegistration, RecoverPassword, FacebookService, CreateUserFacebook, LoginFacebook, $timeout, SendInvitation, LinkFacebook){
        var vm = this;
        vm.credentials = {};
        vm.icono = "!";
        vm.newUser = {
                    "fullName":"",
                    "mail":"",
                    "phoneNumber":"",
                    "job":"",
                    "rol":"",
                    "institutionName":"",
                    "details":""
                };

        vm.login = {
            "username":"",
            "password":""
        };
        vm.newPassword = {
            "password":""
        };

        //$rootScope.tokenLoging = undefined;

        authentication.logout();

        vm.acceptButton = false;
        vm.cancelButton = false;
        vm.okButton = false;
        vm.okGo = false;
        vm.checkButton = false;

        vm.openModalMessage = function(){
            $scope.modalInstance = $uibModal.open({
                templateUrl: 'template/modal/modal_message.html',
                scope: $scope,
                windowClass: 'app-modal-window',
                size: 'sm',
                keyboard  : false
            });
        };

        vm.openAfterLogin = function(){
            $scope.modalInstance = $uibModal.open({
                templateUrl: 'modules/login/partials/modal/afterLogin_modal.html',
                scope: $scope,
                windowClass: 'app-modal-window',
                backdrop: 'static',
                size: 'md',
                keyboard: false
            });
        };

        // consulta si esta realizando una activacion de cuenta
        vm.code = $location.absUrl().split('=')[1];
        if(vm.code != undefined){
            $scope.modalInstance = $uibModal.open({
                templateUrl: 'modules/login/partials/modal/password_activate_modal.html',
                scope: $scope,
                windowClass: 'app-modal-window',
                backdrop: 'static',
                size: 'md',
                keyboard: false
            });
        }

        vm.submit = function() {
                var login = {
                    "username": vm.login.username,
                    "password": vm.login.password
                };
                Login.save(vm.login,
                    function(success){
                        $rootScope.areas = false;
                        /*document.getElementsByTagName('body')[0].style = 'height: auto';*/
                        //Guarda response de login en el cliente
                        authentication.saveToken(success.response.token);
                        authentication.saveUser(success.response.userDetail);
                        authentication.saveStudent(success.response.studentId);
                        $rootScope.tokenLoging = authentication.isLoggedIn();
                            //if(success.response.userDetail.status === "WIZARD")
                                //$state.go('dashboard');
                            if(success.response.userDetail.status === "ACTIVE")
                                if(success.response.userDetail.firstName === null){
                                vm.openAfterLogin();
                                }else{
                                    $state.go('dashboard');
                                    setTimeout(function() { $rootScope.areas = true; }, 2000);
                                }
                    },
                    function(error){
                        if (error.status == 412)
                            vm.message = "Disculpe su usuario '"+vm.login.username + "' se encuentra INACTIVO.";
                        else if (error.status == 401)
                            vm.message = 'Usuario/Clave incorrecto. Por favor intente de nuevo.';
                        else
                            vm.message = 'Disculpe, en estos momentos no puede acceder al sistema. Por favor, revise su usuario y contraseña e intente de nuevo.';

                        vm.title = "Login";
                        vm.message2 = "";
                        vm.acceptButton = false;
                        vm.cancelButton = false;
                        vm.okButton = true;
                        vm.okGo = false;
                        vm.openModalMessage();
                    });


        };

        //LLamada de facebok ver los datos de la connexion a facebook
        vm.testAPI = function() {
            console.log('Welcome!  Fetching your information.... ');
            /*$scope.getMyLastName = function() {
                FacebookService.getMyLastName()
                    .then(function(response) {
                            //$scope.last_name = response.last_name;
                            console.log(response);
                        }
                    );
            };*/
            FB.api('/me?fields=name,email,first_name,last_name,picture', function (response) {
                vm.userFacebook = {
                    "password": response.id,
                    "mail": response.email,
                    "firstName": response.first_name,
                    "lastName": response.last_name,
                    "foto": response.picture.data.url
                    //"mailFacebook": response.email
                };
                vm.loginFacebok = {
                    "username": response.email,
                    "password": response.id
                };
                LoginFacebook.save(vm.loginFacebok,
                    function(success){
                        if(success.message != "No se encontró"){
                            //Guarda response de login en el cliente
                            authentication.saveToken(success.response.token);
                            authentication.saveUser(success.response.userDetail);
                            $rootScope.tokenLoging = authentication.isLoggedIn();
                            $state.go('dashboard');
                        }else{
                            vm.message = "Disculpe, no se encuentró ningún usuario con esta cuenta de Facebook";
                            vm.title = "Login Facebook";
                            vm.acceptButton = false;
                            vm.cancelButton = false;
                            vm.okButton = true;
                            vm.okGo = false;
                            vm.openModalMessage();
                        }

                    },
                    function(error){
                        vm.message = "Disculpe, en estos momentos no puede acceder al sistema.";
                        vm.title = "Login Facebook";
                        vm.acceptButton = false;
                        vm.cancelButton = false;
                        vm.okButton = true;
                        vm.okGo = false;
                        vm.openModalMessage();
                        //CreateUserFacebook.save({},vm.userFacebook,
                        //    function(success){
                        //        vm.facebookLogin();
                        //    });
                    });
            });
        };

        vm.loginFacebook = function(){
            FB.login(function(response){
                scope: 'email,user_birthday,status_update,publish_stream'
                vm.clickFb();

            });
        };

        //LLamada de facebook ver si se connecto
        vm.clickFb = function(){
            FB.getLoginStatus(function (response) {
                if(response.status == "connected")
                    vm.testAPI();
            });
        };

        vm.clickFacebook = function(){
            //FB.getLoginStatus(function (response) {
            //    if(response.status == "connected")
            //        vm.EnlazarFacebook();
            //});
            $scope.modalInstance.close('close');
            vm.message = "Espere mientras se conecta con Facebook.";
            vm.title = "Login Facebook";
            vm.acceptButton = false;
            vm.cancelButton = false;
            vm.okButton = false;
            vm.okGo = false;
            vm.openModalMessage();
            FB.login(function(response){
                scope: 'email,user_birthday,status_update,publish_stream'
                vm.LoginStatus();

            });
        };

        vm.LoginStatus = function(){
            FB.getLoginStatus(function (response) {
                if(response.status == "connected")
                    vm.EnlazarFacebook();
            });
        }

        vm.EnlazarFacebook = function(){
            FB.api('/me?fields=name,email,first_name,last_name,picture', function (response) {
                vm.userFacebook = {
                    "password": response.id,
                    "mail": response.email,
                    "firstName": response.first_name,
                    "lastName": response.last_name,
                    "foto": response.picture.data.url
                    //"mailFacebook": response.email
                };
                vm.loginFacebok = {
                    "username": response.email,
                    "password": response.id
                };
                vm.message = "Se conectó exitosamente a Facebook enlazando al usuario.";
                LinkFacebook.save(vm.userFacebook,
                function(success){
                    authentication.saveUser(success.response);
                    $scope.modalInstance.close('close');
                    $state.go('dashboard');
                },function(error){
                        vm.message = "Error al enlazar Facebook con su usuario.";
                        vm.title = "Login Facebook";
                        vm.acceptButton = false;
                        vm.cancelButton = false;
                        vm.okButton = false;
                        vm.checkButton = true;
                        vm.okGo = false;
                        vm.openModalMessage();
                    });
            });
        };

        vm.openRequestInvitation = function(){
            $scope.modalInstanceRegistro = $uibModal.open({
                templateUrl:
                    'modules/login/partials/modal/requestInvitation_modal.html',
                scope: $scope,
                windowClass: 'app-modal-window',
                keyboard: false
            });
        };

        vm.sendInvitation = function(){
            vm.title = "Solicitud de Invitaci\u00F3n";
            vm.message = 'Solicitud de Invitaci\u00F3n en progreso...';
            vm.message2 = "";
            vm.acceptButton = false;
            vm.cancelButton = false;
            vm.okButton = false;
            vm.openModalMessage();
            vm.invitation.participation =[];
            if(vm.institution){
                vm.invitation.participation.push({name:"Institución"});
            }
            if(vm.profesor){
                vm.invitation.participation.push({name:"Profesor"});
            }
            if(vm.representante){
                vm.invitation.participation.push({name:"Representante"});
            }
            if(vm.estudiante){
                vm.invitation.participation.push({name:"Estudiante"});
            }

                SendInvitation.save({},vm.invitation,
                function(success){
                    //console.log("respuesta registro:"+JSON.stringify(success))
                    vm.message = "Solicitud enviada";
                    //vm.message2 = "La activaci\u00F3n de su cuenta fue enviada al correo electr\u00F3nico";
                    vm.okButton = true;

                    $scope.modalInstanceRegistro.dismiss('cancel');
                    if($scope.modalInstance.result.$$state.status === 2){
                        vm.openModalMessage();
                    }
                },
                function(error){
                    //console.log(error);
                    if(error.data.message.indexOf("Already exists invitation")!=-1){
                        vm.message = "Invitacion ya existe";
                    }else {
                        vm.message = "Error de solicitud de invitaci\u00F3n";
                    }
                    if(error.data.message.indexOf("Already exists user")!=-1){
                        vm.message = "Usuario ya existe";
                    }
                    vm.message2 = "";
                    vm.cancelButton = false;

                    if($scope.modalInstance.result.$$state.status === 2){
                        vm.openModalMessage();
                    }
                });
        };

        vm.openRecoverPass = function(){
            $scope.modalInstanceRecover = $uibModal.open({
                templateUrl:
                    'modules/login/partials/modal/recoverPassword.html',
                scope: $scope,
                windowClass: 'app-modal-window',
                keyboard: false
            });
        };

        vm.recoverPassword = function(){
            // correo a recuperar contraseña:
            //vm.newUser.mail

            vm.title = "Recuperar Contraseña";
            vm.message = 'Recuperación de contraseña en progreso...';
            vm.message2 = "";
            vm.acceptButton = false;
            vm.cancelButton = false;
            vm.okButton = false;
            vm.openModalMessage();

            if(vm.newUser.mail !== undefined){
                RecoverPassword.get({mail:vm.newUser.mail},vm.newUser,
                    function(success){
                        //console.log("respuesta registro:"+JSON.stringify(success))
                        vm.message = "Su contraseña fue enviada a su correo electrónico";
                        vm.okButton = true;

                        $scope.modalInstanceRecover.dismiss('cancel');
                        if($scope.modalInstance.result.$$state.status === 2){
                            vm.openModalMessage();
                        }
                    },
                    function(error){
                        if(error.data != undefined && error.data.message != undefined && error.data.message.indexOf("Parameter invalid")!=-1){
                            vm.message = "El correo no es válido";
                        }else{
                            vm.message = "Error de registro de usuario";
                        }
                        vm.message2 = "";
                        vm.cancelButton = false;

                        if($scope.modalInstance.result.$$state.status === 2){
                            vm.openModalMessage();
                        }
                    });

            }else{
                alert("El correo no puede estar vacío");
            }


        };

        vm.ok = function () {
            $state.reload();
            if($scope.modalInstance !== undefined)
                $scope.modalInstance.close('close');
            if($scope.modalInstanceRegistro !== undefined)
                $scope.modalInstanceRegistro.close('close')
        };

        vm.go = function () {
            $state.go('root');
        };

        vm.cancel = function () {
            $scope.modalInstance.close('close');
        };

        vm.close = function () {
            if($scope.modalInstance !== undefined)
                $scope.modalInstance.close('close');
            if($scope.modalInstanceRegistro !== undefined)
                $scope.modalInstanceRegistro.close('close')
        };

        vm.closeRecover = function () {
            if($scope.modalInstance !== undefined)
                $scope.modalInstance.close('close');
            if($scope.modalInstanceRecover !== undefined)
                $scope.modalInstanceRecover.close('close')
        };

        vm.emailValidation = function(email){
            vm.email = email.$viewValue;
            vm.expresion = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if(!vm.expresion.test(vm.email)){
                vm.error = true;
            }else{
                vm.error = false;
            }
            if(vm.email.length < 6){
                vm.warning = true;
            }else{
                vm.warning = false;
            }
        };

        vm.validatePassword = function(password) {
            vm.password = password;
            if (vm.password != vm.newPassword.password) {
                vm.error = true;
            } else {
                vm.error = false;
            }
        };

        vm.phoneNumberValidation = function(phoneNumber){
            vm.phoneNumber = phoneNumber.$viewValue;
            if(vm.phoneNumber.length < 10){
                vm.errorPhoneNumber = true;
            }else{
                vm.errorPhoneNumber = false;
                if(vm.phoneNumber.length > 10){
                    vm.warningPhoneNumber = true;
                }else{
                    vm.warningPhoneNumberd = false;
                }
            }
        };

        vm.facebookLogin = function(){
            LoginFacebook.save(vm.loginFacebok,
                function(success){
                    //Guarda response de login en el cliente
                    authentication.saveToken(success.response.token);
                    authentication.saveUser(success.response.userDetail);
                    $rootScope.tokenLoging = authentication.isLoggedIn();
                    $state.go('dashboard');
                });
        };

        vm.goToDashboard = function() {
            $state.go('dashboard');
        }

        vm.validationCuenta = function(){

            $scope.modalInstance.close('close');

            vm.title = "Registro";
            vm.message = "Estamos activando su cuenta ...";

            vm.openModalMessage();

            ValidateUserRegistration.save({code: vm.code},vm.newPassword,
                function(success){
                    vm.message = "¡Felicitaciones!";
                    vm.message2 = "Ha completado su registro, su cuenta esta activa";
                    vm.okGo = true;
                    if($scope.modalInstance.result.$$state.status === 2){
                        vm.openModalMessage();
                    }
                },
                function(error){
                    console.log(error);
                    var message = error.data.message;
                    vm.message = "Hubo problemas al activar su cuenta";

                    //The code is not valid, this may be that the code has already been used or does not exist
                    if(message != undefined && message.indexOf("The code is not valid") !== -1) {
                        vm.message = "C\u00F3digo de validaci\u00F3n incorrecto";
                        vm.message = "El c\u00F3digo pudo haber sido utilizado anteriormente y ya no existen en el sistema";
                    }

                    if(message != undefined && message.indexOf("The code is no longer valid") !== -1) {
                        vm.message = "C\u00F3digo de activaci\u00F3n ha expirado";
                        vm.message2 = "Ha esperado mucho tiempo para activar su cuenta. Debes volver a crear la cuenta en el sistema";
                    }
                    vm.okGo = true;
                    if($scope.modalInstance.result.$$state.status === 2){
                        vm.openModalMessage();
                    }
                });
        }

    }

})();
