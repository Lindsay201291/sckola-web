/**
* Controllers
* @namespace Controllers
*/
(function(){

  'use strict';

  angular.module('sckola.community')
  .controller('CommunityCtrl',CommunityCtrl);

  CommunityCtrl.$inject = ['$scope','$state','Communities','ComunnityAssociate','authentication','$rootScope', '$timeout', '$uibModal'];
  /**
  * @namespace CommunityController
  * @desc Controller for community
  * @memberOf Controllers
  */
  function CommunityCtrl($scope, $state, Communities, CommunityAssociate, authentication,$rootScope, $timeout, $uibModal){

    var vm = this;
    vm.user = authentication.getUser();
    vm.communities = [];
    vm.successMessage = "Success!!";
    vm.showSuccessMessage = false;
    vm.messageHelp = "";
    vm.showHelp = false;
    vm.errorMessage = "¡Oops! Algo ha salido mal";
    vm.showError = false;
    var permission = authentication.currentUser();
    vm.community = authentication.getCommunity();
    $rootScope.tokenLoging = authentication.isLoggedIn();
    vm.validarCommunity = true;

    Communities.get({},
      function(success){
        vm.communities = success.response;
      },function(error){
        vm.errorMessage = "Ha ocurrido un error cargando las comunidades";
      });

      if(vm.community==null){
        vm.showHelp = true;
        vm.helpMessage = "Parece que no posees una comunidad. ¡Selecciona una comunidad de la lista desplegable!";
      }

      /**
      * @name showErrorMessage
      * @desc Shows error message to the user
      * @memberOf Controllers.CommunityController
      */
      vm.showErrorMessage = function(){
        vm.showError = true;
        $timeout(function(){
          vm.showError = false;
        },2500);
      };

      /**
      * @name helpUser
      * @desc Depending of the user state, if it has a community or not, a mmater or not,
      *       the state of the view is changed to the requirement.
      * @memberOf Controllers.CommunityController
      */
      vm.helpUser = function(){

      };

      vm.goHome = function(){
          $state.go('dashboard');
      };

      /**
      * @name associateCommunity
      * @desc associates a community to the teacher.
      * @memberOf Controllers.CommunityController
      */
      vm.associateCommunity = function(){
        vm.associate = {associate:true};
        CommunityAssociate.save({communityId:vm.community.id,userId:vm.user.id,roleId:1},vm.associate,function(success){
          vm.successMessage = "¡Felicidades! Has asociado una comunidad exitosamente";
          vm.showSuccessMessage = true;
          if(vm.showHelp){
            vm.showHelp = false;
          }
          vm.mensajeSuccessful = "¡Felicidades! Has asociado una comunidad exitosamente";
          $scope.modalInstance = $uibModal.open({
            templateUrl: 'modules/modals/partials/modal-succefull.html',
            scope: $scope,
            size: 'sm',
            keyboard  : false
          });
          $scope.$parent.$broadcast('newCommunity', success.response);
        },function(error){
          vm.mensajeError = "¡Oops! Algo ha salido mal.";
          $scope.modalInstance = $uibModal.open({
            templateUrl: 'modules/modals/partials/modal-error.html',
            scope: $scope,
            size: 'sm',
            keyboard  : false
          });
          vm.showErrorMessage();
        })
      };

      vm.validarCampos = function(){
        if(vm.community == null || vm.community == ""){
            vm.validarCommunity = true;
        }else{
          vm.validarCommunity = false;
        }
      };

    vm.cerrar = function(){
      $scope.modalInstance.dismiss('cancel');
      vm.goHome();
    };

    vm.salir = function(){
      $scope.modalInstance.dismiss('cancel');
      vm.goHome();
    }

    }
  })();
