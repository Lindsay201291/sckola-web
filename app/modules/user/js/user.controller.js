(function(){
  'use strict';

  angular
  .module('sckola.user')
  .controller('EditProfileCtrl', EditProfileCtrl);

  EditProfileCtrl.$inject = ['authentication', '$rootScope', '$scope', 'UserUpdate', 'UserPhoto', 'UserExperienceEdit', 'UserExperience', '$timeout', '$uibModal'];
  function EditProfileCtrl(authentication, $rootScope, $scope, UserUpdate, UserPhoto, UserExperienceEdit, UserExperience, $timeout, $uibModal) {
    var vm = this;
    vm.message = "Cargando pagina";
    vm.messageTypes = {success: "Success", error: "Error"};
    vm.showMyCommunities = false;
    vm.showSuccessMessage = false;
    vm.showErrorMessage = false;
    vm.successMessage = "¡Se ha actualizado tu perfil exitosamente!";
    vm.mensajeSuccessful = "¡Se ha actualizado tu perfil exitosamente!";
    vm.errorMessage = "¡Oops! Algo ha salido mal.";
    vm.mensajeError = "¡Oops! Algo ha salido mal.";
    vm.errorMessageDate = "La fecha de nacimiento es invalida";

    vm.user = authentication.getUser();
    var permission = authentication.currentUser();
    var idUser = vm.user.id;
    $rootScope.tokenLoging = authentication.isLoggedIn();
      vm.validateFirstName = false;
      vm.validateSecondName = false;
      vm.validateLastName = false;
      vm.validateSecondLastName = false;
      vm.validateBirthDate = false;
      vm.validateGender = false;

    vm.loadInitial = function () {
      UserUpdate.get({userId: idUser},
          function (success) {
            vm.usuario = success.response;
            for (var i = 0; i < vm.usuario.curriculum.length; i++) {
              if (vm.usuario.curriculum[i].date != null) {
                var formatoDate = vm.usuario.curriculum[i].date.split("-");
                vm.usuario.curriculum[i].anio = Number(formatoDate[2]);
              }
            }
            if (vm.usuario.curriculum.length == 0) {
              vm.usuario.curriculum = [{}]
            }
            if (vm.usuario.foto == null) {
              vm.usuario.foto = "/usuario.png";
            }
            if (vm.usuario.birthdate != null) {
              var formato = vm.usuario.birthdate.split("-");
              vm.usuario.birthdateOld = new Date(formato[2], formato[1] - 1, formato[0]);
            }
              vm.validarCampo();
              vm.validarCurriculum();
          },
          function (error) {
            vm.showMessage(vm.messageTypes.error);
          });
    };

    //Valida si el usuario esa autenticado con los permisos para la vista
    if (permission === null || authentication.getUser() === null) {
      $state.go('root');
      console.log("no esta autenticado:" + permission + " JSON:" + permission !== undefined ? JSON.stringify(permission) : "null");
    } else {
      // carga inicial por defecto
      vm.loadInitial();
    }


    $scope.foto = function (imagen) {
      vm.imagen = new FormData();
      vm.imagen.append("file", imagen[0]);
      UserPhoto.save({userId: idUser}, vm.imagen,
          function (success) {
            UserUpdate.update({userId: idUser}, success.response,
                function (success) {
                  vm.usuario = success.response;
                  if (vm.usuario.curriculum.length == 0) {
                    vm.usuario.curriculum = [{}]
                  }
                  $scope.$parent.$broadcast('userSave', success.response);
                    $scope.$parent.$broadcast('user', success.response);
                  if (vm.usuario.birthdate != null) {
                    var formato = vm.usuario.birthdate.split("-");
                    vm.usuario.birthdateOld = new Date(formato[2], formato[1] - 1, formato[0]);
                  }
                  $scope.$parent.$broadcast('userUpdate', success.response);
                });
          },
          function (error) {
            vm.showMessage(vm.messageTypes.error);
          });
    };

      vm.update = function(){
          if (vm.usuario.birthdate != null) {
              var formato = vm.usuario.birthdate.split("-");
              vm.birthdateOld = new Date(formato[2], formato[1] - 1, formato[0]);
          } else {
              vm.birthdateOld = null;
          }
          if (vm.birthdateOld != vm.usuario.birthdateOld) {
              vm.usuario.birthdate = vm.usuario.birthdateOld.getDate() + "-" + (vm.usuario.birthdateOld.getMonth() + 1) + "-" + vm.usuario.birthdateOld.getFullYear();
          }
          if (vm.usuario.firstName != " " && vm.usuario.firstName != null &&
              vm.usuario.lastName != " " && vm.usuario.lastName != null &&
              vm.usuario.gender != null && vm.usuario.birthdate != null) {
              UserUpdate.update({userId: idUser}, vm.usuario,
                  function (success) {
                      vm.usuario = success.response;
                      if (vm.usuario.curriculum.length == 0) {
                          vm.usuario.curriculum = [{}]
                      }else{
                          var formatoDate = success.response.curriculum[0].date.split("-");
                          vm.usuario.curriculum[0].anio = Number(formatoDate[2]);
                      }
                      $scope.$parent.$broadcast('userSave', success.response);
                      if (vm.usuario.birthdate != null) {
                          var formato = vm.usuario.birthdate.split("-");
                          vm.usuario.birthdateOld = new Date(formato[2], formato[1] - 1, formato[0]);
                      }
                      $scope.$parent.$broadcast('userUpdate', success.response);
                      vm.mensajeSuccessful = "¡Se ha actualizado tu perfil exitosamente!";
                      $scope.modalInstanceSuccess = $uibModal.open({
                          templateUrl: 'modules/modals/partials/modal-succefull.html',
                          scope: $scope,
                          size: 'sm',
                          keyboard  : false
                      });
                      vm.showMessage(vm.messageTypes.success);
                  },
                  function (error) {
                      vm.mensajeError = "¡Oops! Algo ha salido mal.";
                      $scope.modalInstanceError = $uibModal.open({
                          templateUrl: 'modules/modals/partials/modal-error.html',
                          scope: $scope,
                          size: 'sm',
                          keyboard  : false
                      });
                      vm.showMessage(vm.messageTypes.error);
                  });
          }
      };


      /**
       * @name showMessage
       * @desc Shows message to user depending of its type
       * @param {string} message Message type
       * @memberOf Controllers.UserController
       */
      vm.showMessage = function (message) {
        switch (message) {
          case "Success":
            vm.showSuccessMessage = true;
            $timeout(function () {
              vm.showSuccessMessage = false;
            }, 2500);
            break;
          case "Error":
            vm.showErrorMessage = true;
            $timeout(function () {
              vm.showErrorMessage = false;
            }, 2500);
            break;
        }
      };

      vm.updateExperience = function (experience) {
        var experienceEdit = angular.copy(experience);
        if (experience.anio != undefined && experience.title != undefined && experience.institute != undefined) {
          if (experience.id == null) {
            experienceEdit.date = "01-01-" + experience.anio.toString();
            delete experienceEdit.anio;
            UserExperience.save({userId: idUser}, experienceEdit,
                function (success) {
                  vm.usuario.curriculum[0] = success.response;
                  if (success.response.date != null) {
                    var formatoDate = success.response.date.split("-");
                    vm.usuario.curriculum[0].anio = Number(formatoDate[2]);
                  }
                    //vm.update();
                  vm.showMessage(vm.messageTypes.success);
                }, function (error) {
                  vm.showMessage(vm.messageTypes.error);
                });
          } else {
            experienceEdit.date = "01-01-" + experience.anio.toString();
            UserExperienceEdit.update({curriculumId: experience.id}, experienceEdit,
                function (success) {
                  vm.showMessage(vm.messageTypes.success);
                }, function (error) {
                  vm.showMessage(vm.messageTypes.error);
                });
          }
        }
      };

      vm.validarFechaDeNacimiento = function () {
        var dateMax = new Date();
        if (vm.usuario.birthdateOld > dateMax) {
          vm.usuario.birthdateOld = null;
            vm.mensajeError = "La fecha de nacimiento es invalida";
            $scope.modalInstanceError = $uibModal.open({
                templateUrl: 'modules/modals/partials/modal-error.html',
                scope: $scope,
                size: 'sm',
                keyboard  : false
            });
          vm.showErrorMessageDate = true;
          $timeout(function () {
            vm.showErrorMessageDate = false;
          }, 2500);
        }
      };

      vm.validationCargaAcademica = function (curriculum) {
        var date = new Date();
        if (curriculum.anio < date.getFullYear() - 50) {
          curriculum.anio = null;
        }

        if (curriculum.anio > date.getFullYear()) {
          curriculum.anio = null;
        }
        vm.updateExperience(curriculum);
      };

      vm.validarCampo = function(){
          if(vm.usuario.firstName == null || vm.usuario.firstName == ""){
              vm.validateFirstName = true;
          }else{
              vm.validateFirstName = false;
            if(vm.usuario.secondName == null || vm.usuario.secondName == ""){
                vm.validateSecondName = true;
            }else{
                vm.validateSecondName = false;
                if(vm.usuario.lastName == null || vm.usuario.lastName == ""){
                    vm.validateLastName = true;
                }else{
                    vm.validateLastName = false;
                    if(vm.usuario.secondLastName == null || vm.usuario.secondLastName == ""){
                        vm.validateSecondLastName = true;
                    }else{
                        vm.validateSecondLastName = false;
                        if(vm.usuario.birthdateOld == null || vm.usuario.birthdateOld == ""){
                            vm.validateBirthDate = true;
                        }else{
                            vm.validateBirthDate = false;
                            if(vm.usuario.gender == null || vm.usuario.gender == ""){
                                vm.validateGender = true;
                            }else{
                                vm.validateGender = false;
                            }
                        }
                    }
                }
            }
          }
      };

      vm.validarCurriculum = function(){
          if(vm.usuario.curriculum[0].anio == null || vm.usuario.curriculum[0].anio == ""){
              vm.validateAnio = true;
          }else{
              vm.validateAnio = false;
              if(vm.usuario.curriculum[0].title == null || vm.usuario.curriculum[0].title == ""){
                 vm.validateTitle = true;
              }else{
                  vm.validateTitle = false;
                  if(vm.usuario.curriculum[0].institute == null || vm.usuario.curriculum[0].institute == ""){
                      vm.validateInstitute = true;
                  }else{
                      vm.validateInstitute = false;
                  }
              }
          }
      };

      vm.cerrar = function(){
          $scope.modalInstanceSuccess.dismiss('cancel');
      };

      vm.salir = function(){
          $scope.modalInstanceError.dismiss('cancel');
      }

    }


})();
