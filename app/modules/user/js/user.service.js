(function(){
    'use strict';

    angular
        .module('sckola.user')
        .factory('ValidateUserRegistration', ValidateUserRegistration)
        .factory('UserRegistration', UserRegistration)
        .factory('UserUpdate',userUpdate)
        .factory('UserExperience',userExperience)
        .factory('UserExperienceEdit', userExperienceEdit)
        .factory('UserPhoto', userPhoto);

    UserRegistration.$inject = ['$resource','$rootScope'];
    function UserRegistration($resource,$rootScope){
        return $resource($rootScope.domainServiceUrl + '/skola/user');
    };


    ValidateUserRegistration.$inject = ['$resource','$rootScope'];
    function ValidateUserRegistration($resource,$rootScope){
        return $resource($rootScope.domainServiceUrl + '/skola/user/validate',{ code: '@code',
            save: {
                method: 'POST'
                /*headers: {
                 Authorization: 'Bearer '+ authentication.getToken()
                 }*/
            },
            get: {
                method: 'GET'
            } ,
            query: {
                method: 'GET',
                isArray:true
            },
            update: {
                method: 'PUT'
            }});
    };

    //SERVICIO PARA ACTUALIZAR LOS DATOS DEL USUARIO
    userUpdate.$inject = ['$resource','$rootScope'];
    function userUpdate($resource,$rootScope){
        return $resource($rootScope.domainServiceUrl+'/skola/user/:userId',null,
            {
                save: {
                    method: 'POST'
                    /*headers: {
                     Authorization: 'Bearer '+ authentication.getToken()
                     }*/
                },
                get: {
                    method: 'GET'
                } ,
                query: {
                    method: 'GET',
                    isArray:true
                },
                update: {
                    method: 'PUT'
                }
            });
    };

    //SERVICIO PARA INGRESAR EXPERIENCIA ACADEMICA
    userExperience.$inject = ['$resource','$rootScope'];
    function userExperience($resource,$rootScope){
        return $resource($rootScope.domainServiceUrl+'/skola/curriculum/user/:userId',null,
            {
                save: {
                    method: 'POST'
                    /*headers: {
                     Authorization: 'Bearer '+ authentication.getToken()
                     }*/
                },
                get: {
                    method: 'GET'
                } ,
                query: {
                    method: 'GET',
                    isArray:true
                },
                update: {
                    method: 'PUT'
                }
            });
    };

    //SERVICIO PARA MODIFICAR LA EXPERIENCIA ACADEMICA
    userExperienceEdit.$inject = ['$resource','$rootScope'];
    function userExperienceEdit($resource,$rootScope){
        return $resource($rootScope.domainServiceUrl+'/skola/curriculum/:curriculumId',null,
            {
                save: {
                    method: 'POST'
                    /*headers: {
                     Authorization: 'Bearer '+ authentication.getToken()
                     }*/
                },
                get: {
                    method: 'GET'
                } ,
                query: {
                    method: 'GET',
                    isArray:true
                },
                update: {
                    method: 'PUT'
                }
            });
    };

    //SERVICIO PARA ACTUALIZAR LOS DATOS DEL USUARIO
    userPhoto.$inject = ['$resource','$rootScope'];
    function userPhoto($resource,$rootScope){
        return $resource($rootScope.domainServiceUrl+'/skola/user/:userId/photo',null,
            {
                save: {
                    method: 'POST',
                    headers: {
                       'Content-Type': undefined
                    }
                },
                get: {
                    method: 'GET'
                } ,
                query: {
                    method: 'GET',
                    isArray:true
                },
                update: {
                    method: 'PUT'
                }
            });
    };


})();
