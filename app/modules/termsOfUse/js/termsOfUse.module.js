(function(){
  'use strict';

  angular
  .module('sckola.termsOfUse',['ui.router'])
  .run(addStateToScope)
  .config(getRoutes)

  addStateToScope.$inject = ['$rootScope', '$state', '$stateParams'];
  /**
  *
  * @name addStateToScope
  * @desc Adds State to Scope.
  * @param {object} $rootScope - RootScope.
  * @param {object} $state - State of the module.
  * @param {object} $stateParams - Params of the state.
  * @memberOf Module.DashboardModule
  */
  function addStateToScope($rootScope, $state, $stateParams){
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
  };

  getRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];
  /**
  *
  * @name getRoutes
  * @desc Gets all the routes of the module
  * @param {object} $rootScope - RootScope.
  * @param {object} $state - State of the module.
  * @param {object} $stateParams - Params of the state.
  * @memberOf Module.DashboardModule
  */
  function getRoutes($stateProvider, $urlRouterProvider){

    $urlRouterProvider.otherwise('/');
    $stateProvider
    .state('termsOfUse', {
      //parent: 'index',
      url: '/termsOfUse',
      views:{
        /*'navbar@': {
          templateUrl: 'modules/login/partials/login.html',
          controller: 'LoginCtrl',
          controllerAs: 'vm'
        },*/
        'termsOfUse@': {
          templateUrl: 'modules/termsOfUse/partials/termsOfUse.html',
          controller: 'TermsOfUseCtrl',
          controllerAs: 'vm'
        }
      }
    })
  };
})();
