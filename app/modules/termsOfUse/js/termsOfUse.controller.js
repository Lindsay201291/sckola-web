(function(){
    'use strict';

    angular
        .module('sckola.termsOfUse')
        .controller('TermsOfUseCtrl', TermsOfUseCtrl);

    TermsOfUseCtrl.$inject = ['$scope', '$state'];
    function TermsOfUseCtrl ($scope, $state){
        var vm = this;
        vm.home = function(){
            $state.go('index');
        };
    }
})();