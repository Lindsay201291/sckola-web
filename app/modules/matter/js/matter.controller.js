/**
* Created by posma.marialejandra on 28/03/2017.
* Controllers
* @namespace Controllers
*/
(function(){
  'use strict';

  angular
  .module('sckola.matter')
  .controller('MatterCtrl', MatterCtrl);

  MatterCtrl.$inject = ['authentication','$rootScope','$scope','$uibModal','$state','Communities','ComunidadMaterias','Matter','SectionByCommunity', 'AssociateMateriaSection', 'AssociateMateriaCommunity', 'CreateSection', 'MattersCommunity', 'DesassociateMatterCommunitySection','$timeout','EvaluationPlanDetail', 'TaskByMatterCommunitySection','TaskCreate', 'TaskGet','TaskCommentGet','TaskCommentCreate', 'TaskDeliveryGetTask', 'TaskDeliveryCreate', 'TaskDeliveryGetTaskUser'];
  /**
  * @namespace MatterController
  * @desc Controller for Matters
  * @memberOf Controllers
  */
  function MatterCtrl(authentication,$rootScope,$scope,$uibModal,$state,Communities,ComunidadMaterias,Matter,SectionByCommunity, AssociateMateriaSection, AssociateMateriaCommunity, CreateSection, MattersCommunity, DesassociateMatterCommunitySection,$timeout,EvaluationPlanDetail, TaskByMatterCommunitySection, TaskCreate, TaskGet,TaskCommentGet,TaskCommentCreate, TaskDeliveryGetTask, TaskDeliveryCreate, TaskDeliveryGetTaskUser) {
      var vm = this;
      vm.messageInitial = "Por favor seleccione una comunidad";
      vm.showMessageInitial = true;
      vm.showError = false;
      vm.errorMessage = "¡Oops! Algo ha salido mal";
      vm.disabledSeccionNew = false;
      vm.staticCommunity = [];
      vm.students = [];
      vm.nameSection = null;
      vm.sectionIntegral = null;
      vm.terminate = false;
      vm.messageHelp = "";
      vm.showHelp = false;
      vm.section = false;
      vm.sectionLabel = false;
      vm.matter = null;
      vm.materia = false;
      vm.matterGeneral = false;
      vm.newSection = false;
      vm.showSuccessMessage = false;
      vm.newMatterMessage = "¡Felicidades! Has Creado una nueva materia";
      vm.removeMatterMessage = "¡Materia Eliminada con Éxito!";
      vm.successMessage = vm.newMatterMessage;
      vm.user = authentication.getUser();
      vm.role = vm.user.roleList[0].name;
      vm.validarCommunitySelect = true;
      vm.validarCommunityNew = true;
      vm.validarMatter = true;
      vm.validarTypeSeccion = true;
      vm.validarSection = true;
      vm.validarSectionIn = true;
      vm.validarSectionCommunity = true;
      vm.previouslyObtainedTask = false;
      vm.allMatters = null;
      vm.hideStudents = true;
      vm.comentario = [];
      vm.taskEdit = false;
      vm.comment = {"user":null,"task":null};
      vm.comentarios = [];
      vm.deliverTask = {"task":null,"user":null,"adjunt":null,"completionDate":null};
      vm.selectedClass = {};

      Communities.get({},
          function(success){
              vm.staticCommunity = success.response;
              vm.loadInitial();
          },
          function(error){
              vm.showErrorMessage();
          });

      vm.user = authentication.getUser();
      vm.communitySelected = authentication.getCommunity();
      var permission = authentication.currentUser();
      $rootScope.tokenLoging = authentication.isLoggedIn();
      if(vm.communitySelected==null){
          vm.showHelp = true;
          vm.helpMessage = "Debe seleccionar una comunidad. ¡Para seleccionar una comunidad haga clic aquí!";
      }
      /**
       * @name showErrorMessage
       * @desc Shows error message to the user
       * @memberOf Controllers.MatterController
       */
      vm.showErrorMessage = function(){
          vm.showError = true;
          $timeout(function(){
              vm.showError = false;
          },2500);
      };
  
     /** 
      * @name differenceBetweenArraysOfMatters
      * @desc Find the difference of two arrays (vm.matters - vm.mattersCommunitys)  
      * @memberOf Controllers.MatterController  
      */  
     vm.differenceBetweenArraysOfMatters = function(matters, mattersCommunitys){  
         var result = [];  
         var matterFound = false;

        for (var i = 0; i < matters.length; i++) {
            for (var j = 0; j < mattersCommunitys.length; j++) {
              if (matters[i].name.localeCompare(mattersCommunitys[j].name) == 0) { 
                matterFound = true;
              }
            }

            if (matterFound == false){
              result.push(matters[i])
            }
      
          matterFound = false
        }

       return result;  
     };  

      /**
       * @name helpUser
       * @desc Depending of the user state, if it has a community or not, a mmater or not,
       *       the state of the view is changed to the requirement.
       * @memberOf Controllers.MatterController
       */
      vm.helpUser = function(){
          if(vm.communitySelected == null){
              $state.go("communities");
          }
      };

      /**
       * @name labelMatterModalTransition
       * @desc Manages transition in modal window for matter label
       * @memberOf Controllers.MatterController
       */

      vm.labelMatterModalTransition = function(){
          if(vm.matterGeneral){
              vm.matterGeneral = false;
              $timeout(function(){
                  vm.materia = false;
              },500);
          }
          else
          if(!vm.materia){
              vm.materia = true;
              $timeout(function(){
                  vm.matterGeneral = true;
              },500);
              vm.allMatters = vm.differenceBetweenArraysOfMatters(vm.matters, vm.mattersCommunitys);
          }
      };

      /**
       * @name sectionTransition
       * @desc Manages transition between combo and textbox for asociate a
       *       section to the matter
       * @memberOf Controllers.MatterController
       */
      vm.sectionTransition = function(){
          vm.sectionLabel = true;
          if(vm.seccion){
              vm.section = false;
              $timeout(function(){
                  vm.newSection = true;
              },500);
          }
          else{
              vm.newSection = false;
              $timeout(function(){
                  vm.section=true;
              },500);
          }
      };

      /**
       * @name loadInitial
       * @desc Loads initial data for the view depending of the selected community
       * @memberOf Controllers.MatterController
       */
      vm.loadInitial = function () {
          if (vm.communitySelected !== undefined && vm.communitySelected.id !== undefined) {
              vm.showMessageInitial = false;
              if(vm.role === "STUDENT"){
                  vm.mattersCommunitysSection(2, vm.user.id);
              }else if(vm.role === "TEACHER"){
                  vm.mattersCommunitysSection(1, vm.user.id);
              }else if(vm.role === "REPRESENTATIVE"){
                  vm.hideStudents = false;
                  vm.students = authentication.getStudent();
                  vm.terminate = true;
              }

              MattersCommunity.get({communityId: vm.communitySelected.communityOrigin.id},
                  function (success) {
                      vm.mattersCommunitys = success.response;
                      if (vm.mattersCommunitys.length == 0) {
                          vm.matterExist = false;
                          vm.messageMatterLoad = "No existen materias asociadas a la comunidad";
                      }
                  },
                  function (error) {
                      vm.messageMatterLoad = "No existen materias asociadas a la comunidad";
                      if (error.errorCode == 'SK-002')
                          vm.matterExist = false;
                      vm.message = "Error en la carga de materias";
                      vm.showMessageInitial = true;
                  }
              );

              SectionByCommunity.get({communityId: vm.communitySelected.communityOrigin.id},
                  function (success) {
                      vm.sectionsByCommunity = success.response;
                  },
                  function (error) {
                      vm.showErrorMessage();
                  }
              )
          }else{
              vm.terminate = true;
              vm.showMessageInitial = true;
          }
      };

      vm.mattersCommunitysSection = function(roleId, userId){
          ComunidadMaterias.get({communityId: vm.communitySelected.communityOrigin.id, roleId: roleId, userId: userId},
              function (success) {
                  vm.communityMatters = success.response;
                  if(vm.communityMatters.length === 0){
                      vm.showHelp = true;
                      if(vm.role === "STUDENT"){
                          vm.helpMessage = "No posees clases en esta comunidad";
                      }else{
                          vm.helpMessage = "No posees clases asociadas a esta comunidad. ¡Haz clic en el botón añadir para asociar una clase!";
                      }

                  }
                  vm.showMessageInitial = false;
                  $timeout(function(){
                      vm.terminate = true;
                  }, 1000);
              },
              function (error) {
                  vm.showErrorMessage();
              }
          );
      };

      vm.mattersCommunitysSectionStudent = function(){
          vm.mattersCommunitysSection(2, vm.student.id);
      };

      /**
       * @name cancel
       * @desc Closes modal window
       * @memberOf Controllers.MatterController
       */
      vm.cancel = function () {
          $scope.modalInstance.dismiss('cancel');
      };

      /**
       * @name ok
       * @desc Closes modal window
       * @memberOf Controllers.MatterController
       */
      vm.ok = function () {
          $scope.modalInstance.dismiss('cancel');
      };

      /**
       * @name go
       * @desc Loads initial view for teacher
       * @memberOf Controllers.MatterController
       */
      vm.go = function () {
          $state.go('teacherHome');
      };

      /**
       * @name matterNew
       * @desc Open modal window to create a new matter
       * @memberOf Controllers.MatterController
       */
      vm.matterNew = function () {
          vm.communitySelect = null;
          vm.matterCommunity = null;
          vm.matter = null;
          vm.sectionByCommunity = null;
          vm.sectionIntegral = null;
          vm.seccion = null;
          vm.nameSection = null;
          vm.newSection = false;
          vm.section = false;
          vm.sectionLabel = false;
          vm.validarCommunitySelect = true;
          vm.validarCommunityNew = true;
          vm.validarMatter = true;
          vm.validarTypeSeccion = true;
          vm.validarSection = true;
          vm.validarSectionIn = true;
          vm.validarSectionCommunity = true;
          Matter.get({},
              function (success) {
                  vm.matters = success.response;
                  $scope.modalInstance = $uibModal.open({
                      templateUrl: 'modules/matter/partials/modal/newMatter.html',
                      scope: $scope,
                      size: 'md',
                      backdrop: 'static',
                      keyboard: false
                  });
              },
              function (error) {
                  vm.showErrorMessage();
              }
          )
      };

      /**
       * @name saveMatter
       * @desc Calls the service that saves matters
       * @memberOf Controllers.MatterController
       */
      vm.saveMatter = function () {
         /* vm.associate = {'associate': true};
          vm.sectionType = {"type": vm.sectionIntegral};
          if (vm.materia) {
              AssociateMateriaCommunity.save({matterId: vm.matter.id, communityId: vm.communitySelected.communityOrigin.id}, vm.associate,
                  function (success) {
                      vm.matterCommunityNew = success.response;
                      if (vm.seccion) {
                          vm.newSection = {"name": vm.nameSection, "comunityId": vm.communitySelected.communityOrigin.id};
                          CreateSection.save({communityId: vm.communitySelected.communityOrigin.id, userId: vm.user.id}, vm.newSection,
                              function (success) {
                                  AssociateMateriaSection.save({
                                          matterCommunityId: vm.matterCommunityNew.id,
                                          sectionId: success.response.id,
                                          userId: vm.user.id
                                      }, vm.sectionType,
                                      function (success) {
                                          vm.sectionIntegral = null;
                                          vm.seccion = null;
                                          vm.nameSection = null;
                                          vm.successMessage = vm.newMatterMessage;
                                          vm.showHelp = false;
                                          vm.showSuccessMessage = true;
                                          $timeout(function(){
                                              vm.showSuccessMessage = false;
                                          }, 2500);
                                          vm.communityMatters.push(success.response);
                                          $scope.modalInstance.dismiss('cancel');
                                          vm.mensajeSuccessful = "¡Felicidades! Has Creado una nueva materia";
                                          $scope.modalInstanceSuccess = $uibModal.open({
                                              templateUrl: 'modules/modals/partials/modal-succefull.html',
                                              scope: $scope,
                                              size: 'sm',
                                              keyboard  : false
                                          });
                                      },
                                      function (error) {
                                          vm.message = "Error en la creacion de clases";
                                          vm.mensajeError = "Error en la creacion de clases";
                                          $scope.modalInstanceError = $uibModal.open({
                                              templateUrl: 'modules/modals/partials/modal-error.html',
                                              scope: $scope,
                                              size: 'sm',
                                              keyboard  : false
                                          });
                                          vm.showErrorMessage();
                                      });
                              },
                              function (error) {
                                  vm.mensajeError = "Error en la creacion de clases";
                                  $scope.modalInstanceError = $uibModal.open({
                                      templateUrl: 'modules/modals/partials/modal-error.html',
                                      scope: $scope,
                                      size: 'sm',
                                      keyboard  : false
                                  });
                                  vm.showErrorMessage();
                              })
                      } else {
                          AssociateMateriaSection.save({
                                  matterCommunityId: vm.matterCommunityNew.id,
                                  sectionId: vm.sectionByCommunity.id,
                                  userId: vm.user.id
                              }, vm.sectionType,
                              function (success) {
                                  vm.sectionIntegral = null;
                                  vm.seccion = null;
                                  vm.sectionByCommunity = null;
                                  vm.successMessage = vm.newMatterMessage;
                                  vm.showSuccessMessage = true;
                                  vm.showHelp = false;
                                  $timeout(function(){
                                      vm.showSuccessMessage = false;
                                  }, 2500);
                                  vm.communityMatters.push(success.response);
                                  $scope.modalInstance.dismiss('cancel');
                                  vm.mensajeSuccessful = "¡Felicidades! Has Creado una nueva materia";
                                  $scope.modalInstanceSuccess = $uibModal.open({
                                      templateUrl: 'modules/modals/partials/modal-succefull.html',
                                      scope: $scope,
                                      size: 'sm',
                                      keyboard  : false
                                  });
                              },
                              function (error) {
                                  vm.message = "Error en la creacion de clases";
                                  vm.mensajeError = "Error en la creacion de clases";
                                  $scope.modalInstanceError = $uibModal.open({
                                      templateUrl: 'modules/modals/partials/modal-error.html',
                                      scope: $scope,
                                      size: 'sm',
                                      keyboard  : false
                                  });
                                  vm.showErrorMessage();
                              });
                      }
                  },
                  function (error) {
                    if (error.data.errorCode == 'SK-005') {
                         vm.mensajeError = "Error en la creacion de la clase: la materia indicada ya pertenece a su comunidad";
                     } else { 
                         vm.mensajeError = "Error en la creacion de la clase";
                     }
                     $scope.modalInstanceError = $uibModal.open({
                      templateUrl: 'modules/modals/partials/modal-error.html', 
                      scope: $scope,
                      size: 'sm',
                      keyboard  : false
                     });
                      vm.showErrorMessage();
                  });
          }*/ 
        var sectionNumber = null;
        vm.associate = {'associate': true};
        vm.sectionType = {"type": vm.sectionIntegral};
          if (vm.materia) {
            if (vm.seccion) {
              vm.newSection = {"name": vm.nameSection, "comunityId": vm.communitySelected.communityOrigin.id};
              CreateSection.save({communityId: vm.communitySelected.communityOrigin.id, userId: vm.user.id}, vm.newSection,
              function (success) {
                sectionNumber = success.response.id;
                AssociateMateriaCommunity.save({matterId: vm.matter.id, communityId: vm.communitySelected.communityOrigin.id}, vm.associate,
                function (success) {
                  vm.matterCommunityNew = success.response;
                  vm.loadInitial();
                  vm.labelMatterModalTransition();
                  AssociateMateriaSection.save({
                  matterCommunityId: vm.matterCommunityNew.id,
                  //sectionId: success.response.id,
                  sectionId: sectionNumber,
                  userId: vm.user.id
                  }, vm.sectionType,
                    function (success) {
                      vm.sectionIntegral = null;
                      vm.seccion = null;
                      vm.nameSection = null;
                      vm.successMessage = vm.newMatterMessage;
                      vm.showHelp = false;
                      vm.showSuccessMessage = true;
                      $timeout(function(){
                        vm.showSuccessMessage = false;
                        }, 2500);
                      vm.communityMatters.push(success.response);
                      $scope.modalInstance.dismiss('cancel');
                      vm.mensajeSuccessful = "¡Felicidades! Has Creado una nueva materia";
                      $scope.modalInstanceSuccess = $uibModal.open({
                        templateUrl: 'modules/modals/partials/modal-succefull.html',
                        scope: $scope,
                        size: 'sm',
                        keyboard  : false
                      });
                    },
                    function (error) {
                      vm.message = "Error en la creacion de clases";
                      vm.mensajeError = "Error en la creacion de clases";
                      $scope.modalInstanceError = $uibModal.open({
                        templateUrl: 'modules/modals/partials/modal-error.html',
                        scope: $scope,
                        size: 'sm',
                        keyboard  : false
                      });
                      vm.showErrorMessage();
                    });
                },
                function (error) {
                  if (error.data.errorCode == 'SK-005') {
                     vm.mensajeError = "Error en la creacion de la clase: la materia indicada ya pertenece a su comunidad";
                   } else { 
                      vm.mensajeError = "Error en la creacion de la clase";
                   }
                   $scope.modalInstanceError = $uibModal.open({
                    templateUrl: 'modules/modals/partials/modal-error.html', 
                    scope: $scope,
                    size: 'sm',
                    keyboard  : false
                   });
                    vm.showErrorMessage();
                  });
              },
              function (error) {
                if (error.data.errorCode == 'SK-005') {
                  vm.mensajeError = "Error en la creacion de la clase: Ya existe la sección indicada selecciónela la lista desplegable";
                  } else {
                    vm.mensajeError = "Error en la creacion de clases";
                  }
                  $scope.modalInstanceError = $uibModal.open({
                    templateUrl: 'modules/modals/partials/modal-error.html',
                    scope: $scope,
                    size: 'sm',
                    keyboard  : false
                    });
                  vm.showErrorMessage();
              })
            }  else {
                AssociateMateriaCommunity.save({matterId: vm.matter.id, communityId: vm.communitySelected.communityOrigin.id}, vm.associate,
                function (success) {
                  vm.matterCommunityNew = success.response;
                  vm.loadInitial();
                  vm.labelMatterModalTransition();
                  AssociateMateriaSection.save({
                  matterCommunityId: vm.matterCommunityNew.id,
                  sectionId: vm.sectionByCommunity.id,
                  userId: vm.user.id
                  }, vm.sectionType,
                  function (success) {
                    vm.sectionIntegral = null;
                    vm.seccion = null;
                    vm.sectionByCommunity = null;
                    vm.successMessage = vm.newMatterMessage;
                    vm.showSuccessMessage = true;
                    vm.showHelp = false;
                    $timeout(function(){
                    vm.showSuccessMessage = false;
                    }, 2500);
                    vm.communityMatters.push(success.response);
                    $scope.modalInstance.dismiss('cancel');
                      vm.mensajeSuccessful = "¡Felicidades! Has Creado una nueva materia";
                      $scope.modalInstanceSuccess = $uibModal.open({
                      templateUrl: 'modules/modals/partials/modal-succefull.html',
                      scope: $scope,
                      size: 'sm',
                      keyboard  : false
                    });
                  },
                  function (error) {
                    vm.message = "Error en la creacion de clases";
                    vm.mensajeError = "Error en la creacion de clases";
                    $scope.modalInstanceError = $uibModal.open({
                      templateUrl: 'modules/modals/partials/modal-error.html',
                      scope: $scope,
                      size: 'sm',
                      keyboard  : false
                      });
                      vm.showErrorMessage();
                  });
                },
                function (error) {
                  if (error.data.errorCode == 'SK-005') {
                    vm.mensajeError = "Error en la creacion de la clase: la materia indicada ya pertenece a su comunidad";
                  } else { 
                      vm.mensajeError = "Error en la creacion de la clase";
                    }
                  $scope.modalInstanceError = $uibModal.open({
                      templateUrl: 'modules/modals/partials/modal-error.html', 
                      scope: $scope,
                      size: 'sm',
                      keyboard  : false
                     });
                      vm.showErrorMessage();
                });
              }
          } else {
              if (vm.seccion) {
                  vm.newSection = {"name": vm.nameSection, "comunityId": vm.communitySelected.communityOrigin.id};
                  CreateSection.save({communityId: vm.communitySelected.communityOrigin.id, userId: vm.user.id}, vm.newSection,
                      function (success) {
                          AssociateMateriaSection.save({
                                  matterCommunityId: vm.matterCommunity.id,
                                  sectionId: success.response.id,
                                  userId: vm.user.id
                              }, vm.sectionType,
                              function (success) {
                                  vm.sectionIntegral = null;
                                  vm.seccion = null;
                                  vm.nameSection = null;
                                  vm.successMessage = vm.newMatterMessage;
                                  vm.showSuccessMessage = true;
                                  vm.showHelp = false;
                                  $timeout(function(){
                                      vm.showSuccessMessage = false;
                                  }, 2500);
                                  vm.communityMatters.push(success.response);
                                  $scope.modalInstance.dismiss('cancel');
                                  vm.mensajeSuccessful = "¡Felicidades! Has Creado una nueva materia";
                                  $scope.modalInstanceSuccess = $uibModal.open({
                                      templateUrl: 'modules/modals/partials/modal-succefull.html',
                                      scope: $scope,
                                      size: 'sm',
                                      keyboard  : false
                                  });
                              },
                              function (error) {
                                  vm.message = "Error en la creacion de clases";
                                  vm.mensajeError = "Error en la creacion de clases";
                                  $scope.modalInstanceError = $uibModal.open({
                                      templateUrl: 'modules/modals/partials/modal-error.html',
                                      scope: $scope,
                                      size: 'sm',
                                      keyboard  : false
                                  });
                                  vm.showErrorMessage();
                              });
                      },
                      function (error) {
                          if (error.data.errorCode == 'SK-005') {
                            vm.mensajeError = "Error en la creacion de la clase: Ya existe la sección indicada selecciónela de la lista desplegable";
                          } else {
                          vm.mensajeError = "Error en la creacion de clases";
                          }
                          $scope.modalInstanceError = $uibModal.open({
                              templateUrl: 'modules/modals/partials/modal-error.html',
                              scope: $scope,
                              size: 'sm',
                              keyboard  : false
                          });
                          vm.showErrorMessage();
                      })
              } else {
                  AssociateMateriaSection.save({
                          matterCommunityId: vm.matterCommunity.id,
                          sectionId: vm.sectionByCommunity.id,
                          userId: vm.user.id
                      }, vm.sectionType,
                      function (success) {
                          vm.sectionIntegral = null;
                          vm.showHelp = false;
                          vm.seccion = null;
                          vm.sectionByCommunity = null;
                          vm.successMessage = vm.newMatterMessage;
                          vm.showSuccessMessage = true;
                          $timeout(function(){
                              vm.showSuccessMessage = false;
                          }, 2500);
                          vm.communityMatters.push(success.response);
                          $scope.modalInstance.dismiss('cancel');
                          vm.mensajeSuccessful = "¡Felicidades! Has Creado una nueva materia";
                          $scope.modalInstanceSuccess = $uibModal.open({
                              templateUrl: 'modules/modals/partials/modal-succefull.html',
                              scope: $scope,
                              size: 'sm',
                              keyboard  : false
                          });
                      },
                      function (error) {
                          vm.message = "Error en la creacion de clases";
                          if (error.data.errorCode == 'SK-005') {
                            vm.mensajeError = "Error en la creacion de la clase: la materia ya existe para la sección indicada";
                          } else {
                          vm.mensajeError = "Error en la creacion de clases";
                          }
                          $scope.modalInstanceError = $uibModal.open({
                              templateUrl: 'modules/modals/partials/modal-error.html',
                              scope: $scope,
                              size: 'sm',
                              keyboard  : false
                          });
                          vm.showErrorMessage();
                      });
              }
          }
      };

      /**
       * @name editMatter
       * @desc Closes modal window
       * @param {object} object matter
       * @memberOf Controllers.MatterController
       */
      vm.editMatter = function (object) {
          $scope.modalInstance = $uibModal.open({
              templateUrl: 'modules/matter/partials/modal/newMatter.html',
              scope: $scope,
              size: 'md',
              keyboard: false
          });
      };

      /**
       * @name matterCommunitySectionDisassociate
       * @desc Desassociate a matter from a community section
       * @param {object} matterCommunitySection matter to desassociate
       * @memberOf Controllers.MatterController
       */
      vm.matterCommunitySectionDisassociate = function(matterCommunitySection){
          vm.associate={associate: false};
          DesassociateMatterCommunitySection.update({
                  matterCommunitySectionId:matterCommunitySection.id
              },vm.associate,
              function(success){
                  var indexOfMatter = vm.communityMatters.indexOf(matterCommunitySection);
                  vm.communityMatters.splice(indexOfMatter, 1);
                  vm.showSuccessMessage = true;
                  vm.successMessage = vm.removeMatterMessage;
                  $timeout(function(){
                      vm.showSuccessMessage = false;
                  }, 2500);
                  vm.mensajeSuccessful = "¡Materia Eliminada con Éxito!";
                  $scope.modalInstanceSuccess = $uibModal.open({
                      templateUrl: 'modules/modals/partials/modal-succefull.html',
                      scope: $scope,
                      size: 'sm',
                      keyboard  : false
                  });
              });
      };

      vm.warnEliminationOfMatter = function(matterCommunitySection){
        vm.matterCommunitySection = matterCommunitySection;
          $scope.modalInstanceWarning = $uibModal.open({
          templateUrl: 'modules/modals/partials/modal-show-question.html',
          scope: $scope,
          size: 'sm',
          keyboard  : false
        });
      };

      vm.selectClass = function(clase) {
          if (vm.previouslyObtainedTask == true) {
              vm.updateSelectedClass();
          }
          vm.clase = clase;
          TaskByMatterCommunitySection.get({matterCommunitySectionId:clase.id},
              function(success) {
                  vm.clase.task = success.response;
                  vm.previouslyObtainedTask == true;
                  vm.updateSelectedClass();
              },
              function(error) {
              });
      };

      vm.updateSelectedClass = function () {
          if (vm.clase.matterCommunity.selected == true) {
              vm.clase.matterCommunity.selected = false;
          } else if (vm.clase.matterCommunity.selected == false || vm.clase.matterCommunity.selected == undefined) {
              vm.clase.matterCommunity.selected = true;
          }
      };

      vm.createTask = function(item){
          vm.task = {"evaluacion":null};
          vm.matterCommunitySection = null;
          vm.completionDate = null;
          vm.taskSelect = false;
          vm.taskEdit = false;
          vm.evaluationTrue = true;
          vm.comentario = [];
          vm.deliveryTask = [];
          vm.selectedClass = item;
          $scope.modalInstance = $uibModal.open({
              templateUrl: 'modules/matter/partials/modal/task_modal.html',
              scope: $scope,
              size: 'lg',
              keyboard  : false
          });
      };

      vm.editTask = function(task){
          vm.task = null;
          vm.comentario = [];
          vm.deliveryTask = [];
          vm.task = task;
          vm.taskSelect = true;
          vm.selectClassTask(vm.task.matterCommunitySectionList[0]);
          vm.getComment();
          vm.DeliveryTaskGetForTask();
          $scope.modalInstance = $uibModal.open({
              templateUrl: 'modules/matter/partials/modal/task_modal.html',
              scope: $scope,
              size: 'lg',
              keyboard  : false
          });
      };

      vm.taskStudent = function(task){
          vm.task = null;
          vm.comentario = [];
          vm.deliveryTask = [];
          vm.task = task;
          vm.deliveryTaskSend = false;
          vm.deliverTask = {"task":null,"user":null,"adjunt":null,"completionDate":null};
          vm.selectClassTask(vm.task.matterCommunitySectionList[0]);
          vm.getComment();
          vm.DeliveryTaskGetForTaskUser();
          $scope.modalInstance = $uibModal.open({
              templateUrl: 'modules/matter/partials/modal/taskStudent_modal.html',
              scope: $scope,
              size: 'lg',
              keyboard  : false
          });
      };

      vm.editTaskRapid = function(){
          vm.taskSelect = false;
          vm.taskEdit = true;
          vm.matterCommunitySection = vm.task.matterCommunitySectionList[0];
          for(var o = 0; o < vm.communityMatters.length; o++){
              if(vm.communityMatters[o].id == vm.task.matterCommunitySectionList[0].id){
                  vm.matterCommunitySection = vm.communityMatters[o];
              }
          }
          var formato = vm.task.completionDate.split("-");
          vm.completionDate = new Date(formato[2], formato[1] - 1, formato[0]);
          vm.selectClassTask(vm.matterCommunitySection);
      };

      vm.selectClassTask = function(item){
          vm.evaluations = null;
          vm.task.evaluacion = null;
          vm.evaluationTrue = false;
          if(item.evaluationPlan != null){
              EvaluationPlanDetail.get({planId: item.evaluationPlan.id},
                  function(success){
                      vm.evaluations = success.response.evaluations;
                      if(vm.task.evaluationId != null){
                          for(var i = 0; i < vm.evaluations.length; i++){
                              if(vm.evaluations[i].id == vm.task.evaluationId){
                                  vm.evaluationTrue = true;
                                  vm.task.evaluacion = vm.evaluations[i];
                              }
                          }
                      }
                  },
                  function(error){
                  });
          }
      };

      //escucha cuando exista cambio en la comunidad seleccionada
      $scope.$on('community', function (evt, community) {
          vm.communitySelected = community;
          if(vm.communitySelected==null){
              vm.showHelp = true;
          }else{
              vm.showHelp = false;
          }
          vm.loadInitial();
      });

      //Valida si el usuario esa autenticado con los permisos para la vista
      if (permission === null || authentication.getUser() === null) {
          $state.go('root');
          console.log("no esta autenticado:" + permission + " JSON:" + permission !== undefined ? JSON.stringify(permission) : "null");
      } else {
      // carga inicial por defecto
      vm.loadInitial();
      }

      vm.validarClase = function(){
          if(vm.communitySelect == null || vm.communitySelect == ""){
              vm.validarCommunitySelect = true;
          }else{
              vm.validarCommunitySelect = false;
          }
      }

      vm.validarClaseNew = function(){
          if(vm.matterCommunity == null || vm.matterCommunity == ""){
              vm.validarCommunityNew = true;
          }else{
              vm.validarCommunityNew = false;
          }
      }

      vm.validarClaseOld = function(){
          if(vm.matter == null || vm.matter == ""){
              vm.validarMatter = true;
          }else{
              vm.validarMatter = false;
          }
      }

      vm.validarTipoSeccion = function(){
          if(vm.seccion == null || vm.seccion == "" || (vm.seccion != false && vm.seccion != true)){
              vm.validarTypeSeccion = true;
          }else{
              vm.validarTypeSeccion = false;
          }
      };

      vm.validarSectionIntegral = function(){
          if(vm.sectionIntegral == null || vm.sectionIntegral == ""){
              vm.validarSection = true;
          }else{
              vm.validarSection = false;
          }
      };

      vm.validarSectionselect = function(){
          if(vm.sectionByCommunity == null || vm.sectionByCommunity == ""){
              vm.validarSectionCommunity = true;
          }else{
              vm.validarSectionCommunity = false;
          }
      };

      vm.validarSectioninput = function(){
          if(vm.nameSection == null || vm.nameSection == ""){
            vm.validarSectionIn = true;
          }else{
              vm.validarSectionIn = false;
          }
      }

      vm.cerrar = function(){
          $scope.modalInstanceSuccess.dismiss('cancel');
      };

      vm.salir = function(){
          $scope.modalInstanceError.dismiss('cancel');
      };

      vm.close = function(){
          $scope.modalInstanceWarning.dismiss('cancel');
      };

      vm.cambiarValorInicialSeccion = function(){ 
        vm.sectionByCommunity = null;
        vm.nameSection = null;
        vm.validarSectionIn = true; 
      };

      vm.cambiarValorInicialMateria = function(){ 
        vm.validarCommunityNew = true;
        vm.matterCommunity = "";
        vm.matter = "";
        vm.validarMatter = true; 
      };

      vm.planEvaluation = function(matter){
          vm.evaluationId = matter.evaluationPlan.id;
          vm.namePlan = matter.evaluationPlan.name;
          vm.evaluationPlanDetail = matter.evaluationPlan;
          vm.cuentaValor = 0;
          EvaluationPlanDetail.get({planId: vm.evaluationId},
              function(success){
                  vm.evaluations = success.response.evaluations;
                  for(var b=0;b < vm.evaluations.length;b++){
                      var formato = vm.evaluations[b].date.split("-");
                      vm.evaluations[b].dateNew = new Date(formato[2],formato[1]-1,formato[0]);
                      vm.cuentaValor = parseInt(vm.cuentaValor)+ parseInt(vm.evaluations[b].weight);
                  }
                  $scope.modalInstance = $uibModal.open({
                      templateUrl: 'modules/evaluation/partials/modal/detailEvaluationPlanStudent_modal.html',
                      scope: $scope,
                      size: 'lg',
                      keyboard  : false
                  });
              },
              function(error){
              });
      };

      vm.cancel = function () {
          $scope.modalInstance.dismiss('cancel');
      };

      vm.createTaskFinish = function(){
          vm.task.matterCommunitySectionList = [];
          vm.task.matterCommunitySectionList.push(vm.matterCommunitySection);
          var dates = new Date();
          vm.task.creationDate = dates.getDate() + "-"+ (dates.getMonth() + 1) +"-"+ dates.getFullYear();
          vm.task.completionDate = vm.completionDate.getDate() + "-"+ (vm.completionDate.getMonth() + 1) +"-"+ vm.completionDate.getFullYear();
          if(vm.task.evaluacion != null){
              vm.task.evaluationId = vm.task.evaluacion.id;
          }else{
              vm.task.evaluationId = null;
          }
          TaskCreate.save(vm.task,
              function (success) {
                  vm.task.id = success.response.id;
                  vm.taskSelect = true;
                  vm.selectClass(vm.selectedClass);
              },function(error){
                  vm.mensajeError = "¡Oops! Algo ha salido mal.";
                  $scope.modalInstanceError = $uibModal.open({
                      templateUrl: 'modules/modals/partials/modal-error.html',
                      scope: $scope,
                      size: 'sm',
                      keyboard  : false
                  });
                  vm.showErrorMessage();
              });
      };

      vm.editTaskFinish = function(){
          vm.task.completionDate = vm.completionDate.getDate() + "-"+ (vm.completionDate.getMonth() + 1) +"-"+ vm.completionDate.getFullYear();
          if(vm.task.evaluacion != null){
              vm.task.evaluationId = vm.task.evaluacion.id;
          }else{
              vm.task.evaluationId = null;
          }
          TaskCreate.update(vm.task,
              function (success) {
                  vm.taskSelect = true;
                  vm.taskEdit = false;
              },function(error){
                  vm.mensajeError = "¡Oops! Algo ha salido mal.";
                  $scope.modalInstanceError = $uibModal.open({
                      templateUrl: 'modules/modals/partials/modal-error.html',
                      scope: $scope,
                      size: 'sm',
                      keyboard  : false
                  });
                  vm.showErrorMessage();
              });
      };

      vm.getComment = function(){
          TaskCommentGet.get({'idTask':vm.task.id},
          function(success){
              vm.comentario = success.response;
              for(var i = 0;i<vm.comentario.length; i++){
                  if(vm.comentario[i].user.foto == null || vm.comentario[i].user.foto == ""){
                      vm.comentario[i].user.foto = "usuario.png";
                  }
              }
          },function(error){
                  vm.mensajeError = "¡Oops! Algo ha salido mal.";
                  $scope.modalInstanceError = $uibModal.open({
                      templateUrl: 'modules/modals/partials/modal-error.html',
                      scope: $scope,
                      size: 'sm',
                      keyboard  : false
                  });
                  vm.showErrorMessage();
              });
      };

      vm.createComment = function(){
          vm.comment.user = vm.user;
          vm.comment.task = vm.task;
          vm.comment.date = new Date();
          TaskCommentCreate.save(vm.comment,
          function(success){
              vm.getComment();
              vm.comment = null;
          },function(error){
                  vm.mensajeError = "¡Oops! Algo ha salido mal.";
                  $scope.modalInstanceError = $uibModal.open({
                      templateUrl: 'modules/modals/partials/modal-error.html',
                      scope: $scope,
                      size: 'sm',
                      keyboard  : false
                  });
                  vm.showErrorMessage();
              });
      };

      vm.DeliveryTaskGetForTask = function(){
          vm.deliveryTaskSend = false;
          TaskDeliveryGetTask.get({'idTask':vm.task.id},
          function(success){
              vm.deliveryTask = success.response;
              for(var i = 0; i< vm.deliveryTask.length; i++){
                  if(vm.deliveryTask[i].user.id = vm.user.id){
                      vm.deliveryTaskSend = true;
                      vm.deliverTask = vm.deliveryTask[i];
                  }
              }
          },function(error){
                  vm.mensajeError = "¡Oops! Algo ha salido mal.";
                  $scope.modalInstanceError = $uibModal.open({
                      templateUrl: 'modules/modals/partials/modal-error.html',
                      scope: $scope,
                      size: 'sm',
                      keyboard  : false
                  });
                  vm.showErrorMessage();
              });
      };

      vm.DeliveryTaskGetForTaskUser = function(){
          vm.deliveryTaskSend = false;
          TaskDeliveryGetTaskUser.get({'idTask':vm.task.id, 'idUser':vm.user.id},
              function(success){
                  vm.deliverTask = success.response;
                  if(vm.deliverTask != null){
                    vm.deliveryTaskSend = true;
                  }
                  if(vm.deliverTask.value == null){
                      vm.deliverTask.value = "Sin evaluar";
                  }
              },function(error){
                  vm.mensajeError = "¡Oops! Algo ha salido mal.";
                  $scope.modalInstanceError = $uibModal.open({
                      templateUrl: 'modules/modals/partials/modal-error.html',
                      scope: $scope,
                      size: 'sm',
                      keyboard  : false
                  });
                  vm.showErrorMessage();
              });
      };

      vm.sendTask = function(){
          vm.deliverTask.task = vm.task;
          vm.deliverTask.user = vm.user;
          var date = new Date();
          vm.deliverTask.completionDate = date.getDate() + "-"+ (date.getMonth() + 1) +"-"+ date.getFullYear();
          TaskDeliveryCreate.save(vm.deliverTask,
          function(success){
              vm.deliveryTaskSend = true;
              vm.deliverTask.value = "Sin evaluar";
          },function(error){
                  vm.mensajeError = "¡Oops! Algo ha salido mal.";
                  $scope.modalInstanceError = $uibModal.open({
                      templateUrl: 'modules/modals/partials/modal-error.html',
                      scope: $scope,
                      size: 'sm',
                      keyboard  : false
                  });
                  vm.showErrorMessage();
              });
      };

      vm.deleteComment = function(comment){
          vm.commentSelect = comment;
          TaskCommentCreate.delete({'idComment':comment.id},
              function(success){

                  for(var i=0; i< vm.comentario.length; i++){
                      if(vm.comentario[i] == vm.commentSelect){
                          vm.comentario.splice(i,1);
                      }
                  }
              });
      };

      vm.deleteTask = function(){
          TaskGet.delete({'idTask':vm.task.id},
          function(success){
              vm.selectClassTask(vm.task.matterCommunitySectionList[0]);
              $scope.modalInstance.dismiss('cancel');
              vm.selectClass(vm.selectedClass);
          },function(error){
                  vm.mensajeError = "No se puede eliminar la tarea.";
                  $scope.modalInstanceError = $uibModal.open({
                      templateUrl: 'modules/modals/partials/modal-error.html',
                      scope: $scope,
                      size: 'sm',
                      keyboard  : false
                  });
                  vm.showErrorMessage();
              });
      }
  }
})();
