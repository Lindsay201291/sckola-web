(function(){
  'use strict';

  angular.module('sckola')
  .controller('NavBarCtrl',NavBarCtrl);

  NavBarCtrl.$inject = ['$scope','$state','$window','CommunityUser','authentication','NotificationHistory', 'RemoveNotification', '$rootScope', 'NewsWizard', '$timeout', 'NotificationInfoService', '$uibModal'];
  function NavBarCtrl($scope,$state,$window,CommunityUser,authentication,NotificationHistory, RemoveNotification, $rootScope, NewsWizard, $timeout, NotificationInfoService, $uibModal){
    var vm = this;
    var permission = authentication.currentUser();
    var sumProgress = 0;
      vm.mensage = "No hay notificaciones pendientes";
      //vm.urlToReports = "#";
      vm.urlToHelp = $rootScope.domainUrlHelp;
      vm.urlToApk = $rootScope.domainUrlApk;

    vm.user = authentication.getUser();
    vm.community = authentication.getCommunity();
    vm.student = authentication.getStudent();
    vm.role = vm.user.roleList[0].name;
      vm.communitiesUser = [];

    vm.loadInitial = function(){
        $rootScope.areas = true;
        if(vm.role === "REPRESENTATIVE"){
            for(var i = 0;i < vm.student.length;i++){
                vm.getCommunity(vm.student[i].id, 2);
            }
        }else if(vm.role === "STUDENT"){
            vm.getCommunity(vm.user.id, 2);
        }else if(vm.role === "TEACHER"){
            vm.getCommunity(vm.user.id, 1);
        }
      vm.news = [];
      vm.notifications = 0;
      vm.showNotificationCount = false;
      vm.activeCommunity = authentication.getCommunity();
      NotificationInfoService.get(
        function(success){
            vm.news = success.response.notifications;
            //vm.notifications += success.response.activeNotifications;
            vm.notifications = success.response.activeNotifications;
            /*if(vm.activeCommunity != null){
                NotificationHistory.get({communityId: vm.activeCommunity.communityOrigin.id},
                    function(success){
                        for(var a=0;a < success.response.notifications.length; a++){
                            vm.news.push(success.response.notifications[a]);
                        }
                        vm.notifications += success.response.activeNotifications;
                    },function(error){

                })
            }*/
        },function(error){
                
        });
    };

     vm.getCommunity = function(user, role){
         CommunityUser.get({userId:user,roleId:role},null,
             function(success){
                 if (success && success.response!= null && success.response.length>0) {
                     for(var o = 0;success.response.length > o;o++){
                         if(vm.communitiesUser.length > 0){
                             vm.communities(success.response[o]);
                         }else{
                             vm.communitiesUser.push(success.response[o]);
                         }
                     }
                     //vm.communitiesUser = success.response;
                     if(authentication.getCommunity() == undefined){
                         vm.communitySelected = vm.communitiesUser[0];
                         vm.sendEvent(vm.communitySelected);
                     }else{
                         vm.communitySelected = authentication.getCommunity();
                     }
                 }
             },
             function(error){
                 vm.message = "Error cargando mis comunidades";
                 vm.communitiesUser = undefined;
             });
     };

     vm.communities = function(community){
         var encontro = false;
         for(var k = 0;vm.communitiesUser.length > k; k++){
             if(vm.communitiesUser[k].name === community.name){
                 encontro = true;
             }
         }
         if(encontro == false){
             vm.communitiesUser.push(community);
         }
     };

    vm.clickNotification = function(notification){
            if(notification.from == "Comunidad"){
                $state.go('communities');
            }
            if(notification.from == "Materia"){
                $state.go('matterHome');
            }
            if(notification.from == "Estudiantes"){
                $state.go("students");
            }
            if(notification.from == "Perfil"){
                $state.go("editProfile");
            }
            if(notification.from == "Plan de Evaluación"){
                $state.go("evaluationPlan");
            }
            if(notification.from == "Asistencia"){
                $state.go("assistances");
            }
            if(notification.from == "Calificaciones"){
                if(vm.role === "TEACHER") {
                    $state.go("uploadNotes");
                } else if(vm.role === "STUDENT") {
                    $state.go("uploadNotesStudent");
                    RemoveNotification.update({notificationId:notification.id},
                        function(success){},function(error){});
                }
            }
        };


      vm.resetCount = function(){
          vm.notifications = 0;
      };

      vm.goToProfile = function(){
          $state.go("editProfile");
      };

      $scope.$on('community', function (evt, community) {
        vm.community = community;
        //vm.urlToReports = $rootScope.domainReportUrl+"Reports/$"+vm.user.id+"$"+vm.community.communityOrigin.id;
        vm.urlToReports = $rootScope.domainReportUrl+"?user="+vm.user.id+"&community="+vm.community.communityOrigin.id;
        //vm.urlToReports = $rootScope.domainReportUrl+"app/#/noAttendance?user="+vm.user.id+"&community="+vm.community.communityOrigin.id;
      });

      if(vm.community != null){
        //vm.urlToReports = $rootScope.domainReportUrl+"Reports/$"+vm.user.id+"$"+vm.community.communityOrigin.id;
        vm.urlToReports = $rootScope.domainReportUrl+"?user="+vm.user.id+"&community="+vm.community.communityOrigin.id;
        //vm.urlToReports = $rootScope.domainReportUrl+"app/#/noAttendance?user="+vm.user.id+"&community="+vm.community.communityOrigin.id;
      }

      vm.goHome = function(){
          $state.go('dashboard');
      };

      vm.goToCommunities = function(){
        $state.go('communities');
      };

      vm.goToCourses = function(){
        $state.go('matterHome');
      };

      vm.goToCoursesStudents = function(){
          $state.go('matterStudent');
      };

      vm.goToAssistances = function(){
        $state.go("assistances");
      };

      vm.goToNotesStudents = function(){
          $state.go("uploadNotesStudent");
      };

      vm.goToStudents = function(){
        $state.go("students");
      };

      vm.gotToEvaluations = function(){
        $state.go("evaluationPlan");
      };

      vm.goToUploadNotes = function(){
        $state.go("uploadNotes");
      };

      vm.goToRoot = function(){
          $scope.modalInstance = $uibModal.open({
              templateUrl: 'modules/modals/partials/modal.html',
              scope: $scope,
              //backdrop: 'static',
              windowClass: 'modal-position',
              size: 'sm',
              keyboard: false
          });
      };

      vm.showMessage = function(){
          vm.messageToUser = "Usted aún no se ha unido a ninguna comunidad";
          $scope.modalInstance = $uibModal.open({
            templateUrl: 'modules/modals/partials/modal-show-message.html',
            scope: $scope,
            size: 'sm',
            keyboard  : false
          });
      };

      vm.cerrar = function(){
          $scope.modalInstance.dismiss('cancel');
      };

      vm.salir = function(){
           $rootScope.tokenLoging = undefined;
           //$state.go("root");
           $window.location.href = '/' ;
           $scope.modalInstance.dismiss('cancel');
      };

      vm.setCommunitySelected = function(community){
        vm.communitySelected = community;
        vm.sendEvent(vm.communitySelected);
      };

      vm.sendEvent = function(communitySelected) {
        authentication.setCommunity(communitySelected);
        $scope.$parent.$broadcast('community', communitySelected);
      };


      $scope.$on('community', function (evt, community) {
          vm.activeCommunity = community;
          vm.messageInitial = "Por favor seleccione una comunidad";
          vm.loadInitial();
      });

      $scope.$on('newCommunity', function(evt, community){
        vm.communitiesUser.push(community);
      });

      var bar = new ProgressBar.Line("#progress", {
              strokeWidth: 4,
              easing: 'easeInOut',
              duration: 1400,
              color: '#75ABDD',
              trailColor: '#eee',
              trailWidth: 1,
              svgStyle: {width: '100%', height: '100%'},
              text: {
                  style: {
                      // Text color.
                      // Default: same as stroke color (options.color)
                      color: '#FFFFFF',
                      position: 'absolute',
                      right: '0',
                      padding: 0,
                      margin: 0,
                      transform: null
                  },
                  autoStyleContainer: false
              },
              from: {color: '#aaa'},
              to: {color: '#75ABDD'},
              step: function(state, bar) {
              bar.setText(Math.round(bar.value() * 100) + ' %');
            }
      });
      bar.text.style.fontFamily = '"Raleway", Helvetica, sans-serif';
      bar.text.style.fontSize = '2rem';
      //Para modificar el progreso hay que configurarlo entre 0.0 y 1.0
      //bar.animate(1.0);

      NewsWizard.get({userId: vm.user.id},
          function(success){

              vm.usuario = success.response;

              if(vm.usuario.profileUser != null){
                  sumProgress = sumProgress + 0.25;
              }
              if(vm.usuario.community != null){
                  sumProgress = sumProgress + 0.25;
              }
              if(vm.usuario.evaluationPlan != null){
                  sumProgress = sumProgress + 0.25;
              }
              if(vm.usuario.matterCommunitySection != null){
                  sumProgress = sumProgress + 0.25;
              }

              bar.animate(sumProgress);

          },
          function(error){

          });



      if (permission === null || authentication.getUser() === null) {
              // user is now logged out
              $state.go('root');
          //$state.go('root');
          console.log("no esta autenticado:" + permission + " JSON:" + permission !== undefined ? JSON.stringify(permission) : "null");
      } else {
          //Loads initial data of the controller
          vm.loadInitial();
      }

      }

    })();
