(function(){
  'use strict';

  angular
  .module('sckola.students')
  .controller('StudentCTRL', StudentCTRL);

  StudentCTRL.$inject = ['authentication','$scope', '$rootScope', '$state','ComunidadMaterias','AddStudentToSection','CreateStudentCommunty','StudentsBySection', 'RemoveStudentToSection', 'StudentsByCommunityNotSection', '$timeout','$uibModal'];
  function StudentCTRL(authentication, $scope, $rootScope, $state,ComunidadMaterias,AddStudentToSection,CreateStudentCommunty, StudentsBySection, RemoveStudentToSection, StudentsByCommunityNotSection, $timeout, $uibModal){
    var vm = this;
    vm.gender=["Femenino","Masculino"];
    vm.studentInvitation = [];
    vm.messageInvitedStudents = "¡Felicitaciones! Has invitado exitosamente estudiantes a tu clase";
    vm.messageDessasociateStudent = "¡Estudiante eliminado de la clase!";
    vm.messageInitial = "Por favor seleccione una comunidad";
    vm.showError = false;
    vm.errorMessage = "¡Oops! Algo ha salido mal";
    vm.showMessageInitial = true;
    vm.invitationStudent = false;
    vm.messageHelp = "";
    vm.showSuccessMessage = false;
    vm.showHelp = false;
    vm.successMessage = "";
    vm.clase = {"section":{"countStudent":null}};
    vm.student = {"identification":null,"firstName":null,"secondName":null,"lastName":null,"secondLastName":null,
                  "date":null,"gender":null,"mail":null,"mailRepresentative":null};
    vm.user = authentication.getUser();
    vm.role = vm.user.roleList[0].name;
    
    var permission = authentication.currentUser();
    vm.community = authentication.getCommunity();
    vm.previouslyObtainedStudents = false;
    $rootScope.tokenLoging = authentication.isLoggedIn();


    if(vm.community==null){
      vm.showHelp = true;
      vm.helpMessage = "Debe seleccionar una comunidad. ¡Para seleccionar una comunidad haga clic aquí!";
    }


    /**
    * @name helpUser
    * @desc Depending of the user state, if it has a community or not, a mmater or not,
    *       the state of the view is changed to the requirement.
    * @memberOf Controllers.StudentController
    */
    vm.helpUser = function(){
      if(vm.community == null){
        $state.go("communities");
      }
      else if (vm.communityMatters.length === 0){
        $state.go("matterHome");
      }
    };

    vm.loadInitial = function(){
      vm.showMessageInitial = false;
      if (vm.community !== undefined && vm.community !== undefined) {
        ComunidadMaterias.get({communityId:vm.community.communityOrigin.id,roleId:vm.user.roleList[0].id,userId:vm.user.id},
          function(success){
            vm.communityMatters = success.response;
            if(vm.communityMatters.length === 0){
              vm.showHelp = true;
              vm.helpMessage= "No posees clases asociadas en esta comunidad. ¡Para asociar una clase haga click aquí!";
            }
            vm.showMessageInitial = false;
            for(var i=0;i<vm.communityMatters.length;i++){
              vm.communityMatters[i].status = false;
            }
            vm.validarCampos();
            $timeout(function(){
              vm.terminate = true;
            }, 1000);
          },
          function(error){
            vm.error = "Error cargado data de comunidad";
            vm.showMessageInitial = true;
            vm.showErrorMessage();
            $timeout(function(){
              vm.terminate = true;
            }, 1000);
          }
        );
      }else{
        vm.terminate = true;
        vm.showMessageInitial = true;
      }
    };



    //Valida si el usuario esa autenticado con los permisos para la vista
    if (permission === null || authentication.getUser() === null) {
      $state.go('root');
      console.log("no esta autenticado:" + permission + " JSON:" + permission!==undefined?JSON.stringify(permission):"null");
    }else{
      // carga inicial por defecto
      vm.loadInitial();
    }

    //escucha cuando exista cambio en la comunidad seleccionada
    $scope.$on('community', function (evt, community) {
      vm.community = community;
      if(vm.community==null){
        vm.showHelp = true;
      }else{
        vm.showHelp = false;
      }
      vm.loadInitial();
    });


    vm.editStudent = function(student) {
      vm.student=student;

      $scope.modalInstance = $uibModal.open({
        templateUrl: 'modules/students/partials/newStudent.html',
        scope: $scope,
        size: 'md',
        keyboard  : false
      });
    };

    vm.showDeleteModal = function(student){
      vm.title = 'Eliminar Estudiante';
      vm.message = '¿Desea eliminar el estudiante: "' + student.name  + ' ' +student.lastName+ '" de la lista?';
      vm.message2='';
      vm.acceptButton = true;
      vm.cancelButton = true;
      vm.okButton = false;
      vm.okGo = false;
      vm.studentSelected = student;
      $scope.modalInstanceDel = $uibModal.open({
        templateUrl: 'template/modal/modal_message.html',
        scope: $scope,
        size: 'sm',
        keyboard  : false
      });
    };

    vm.accept = function (){
      vm.title = '?xito';
      vm.message = 'Estudiante eliminado exitosamente';
      vm.message2='';
      vm.acceptButton = false;
      vm.cancelButton = false;
      vm.okButton = true;
      vm.okGo = false;

      $scope.modalInstanceDel.dismiss('cancel');
      vm.idStudent = vm.studentSelected.id;

      for(vm.i=0 ; vm.i<vm.students.length; vm.i++)
      {
        if (vm.students[vm.i].id == vm.idStudent)
        {
          vm.students.splice(vm.i,1);
        }
      }
      $scope.modalInstance = $uibModal.open({
        templateUrl: 'template/modal/modal_message.html',
        scope: $scope,
        size: 'sm',
        keyboard  : false
      });
    };

    vm.updateSelectedClass = function () {
      if (vm.clase.matterCommunity.selected == true) {
        vm.clase.matterCommunity.selected = false;
      } else if (vm.clase.matterCommunity.selected == false || vm.clase.matterCommunity.selected == undefined) {
        vm.clase.matterCommunity.selected = true;
      }
    }

    vm.selectClass = function(clase) {
      if (vm.previouslyObtainedStudents == true) {
        vm.updateSelectedClass();
      }
      vm.clase = clase;
      StudentsBySection.get({id:clase.section.id},
        function(success) {
          vm.clase.students = success.response;
          vm.previouslyObtainedStudents == true;
          vm.updateSelectedClass();
        },
        function(error) {
          vm.showErrorMessage();
        });
     };

    vm.createStudent = function(){
      vm.student.birthdate = vm.student.date.getDate()+"-"+(vm.student.date.getMonth()+1)+"-"+vm.student.date.getFullYear();
      if(vm.student.gender == "Masculino"){
        vm.student.gender="MALE";
      }else{
        vm.student.gender="FEMALE";
      }
      var validar = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
      if (validar.test(vm.student.mail)){
        CreateStudentCommunty.save({roleId:2,communityId:vm.community.communityOrigin.id},vm.student,
            //CreateStudentCommunty.save({roleId:2,communityId:vm.community.communityOrigin.id},vm.student,
            function(success){
              vm.mensajeSuccessful = "¡Estudiante creado satisfactoriamente!";
              vm.create = true;
              $scope.modalInstanceSuccess = $uibModal.open({
                templateUrl: 'modules/modals/partials/modal-succefull.html',
                scope: $scope,
                size: 'sm',
                keyboard  : false
              });
            },function(error){
              vm.mensajeError = "Error en la creacion de clases";
              $scope.modalInstanceError = $uibModal.open({
                templateUrl: 'modules/modals/partials/modal-error.html',
                scope: $scope,
                size: 'sm',
                keyboard  : false
              });
              vm.showErrorMessage();
            });
      } else {
        vm.showMessageEmail = true;
        vm.messageEmail = "La direccion de Email es invalido";
        vm.mensajeError = "La direccion de Email es invalido";
        $scope.modalInstanceError = $uibModal.open({
          templateUrl: 'modules/modals/partials/modal-error.html',
          scope: $scope,
          size: 'sm',
          keyboard  : false
        });
        $timeout(function(){
          vm.showMessageEmail = false;
        },2500);
      }

  };

  vm.cancel = function () {
    $scope.modalInstance.dismiss('cancel');
  };

  vm.ok = function () {
    $scope.modalInstance.dismiss('cancel');
  };

  vm.go = function(){
    $state.go('teacherHome');
  };

  vm.newMatter = function (name){
    vm.studentName= name;
    $scope.modalInstance = $uibModal.open({
      templateUrl: 'modules/matter/partials/modal/newMatter.html',
      scope: $scope,
      size: 'md',
      keyboard  : false
    });
  };

  vm.studentMatterInvitation = function (classe){
    vm.claseInvitacion = classe;
    vm.studentInvitation = [];
    StudentsByCommunityNotSection.get({communityId:vm.community.communityOrigin.id,sectionId:classe.section.id},
        //StudentsByCommunityNotSection.get({communityId:vm.community.communityOrigin.id,sectionId:classe.section.id},
        function(success){
          vm.students = success.response;
        });
    $scope.modalInstance = $uibModal.open({
      templateUrl: 'modules/student/partials/modal/studentMatterInvitation_modal.html',
      scope: $scope,
      size: 'md',
      keyboard  : false
    });
  };

  vm.Invitation = function(student){
    student.asignado = true;
    vm.studentInvitation.push(student);
    vm.invitationStudent = true;
  };

  vm.quitarInvitacion = function(student){
    student.asignado = false;
    var indexOfStudent = vm.studentInvitation.indexOf(student);
    vm.studentInvitation.splice(indexOfStudent, 1);
    if(vm.studentInvitation.length < 1){
      vm.invitationStudent = false;
    }
  };

  vm.addstudentSection = function(){
    vm.counter = 0;
    for(var i=0; i < vm.studentInvitation.length;i++){
      vm.addStudentClass(vm.studentInvitation[i]);
    }
    $scope.modalInstance.dismiss('cancel');
  };

  vm.addStudentClass = function(student){
    AddStudentToSection.save({userId:student.id,sectionId:vm.claseInvitacion.section.id},null,
        function(success){
          vm.claseInvitacion.students.push(student);
          vm.counter = vm.counter + 1;
          student.asignado = false;
          vm.claseInvitacion.section.countStudent = vm.claseInvitacion.section.countStudent+1;
          vm.mensajeSuccessful = "¡Felicitaciones! Has invitado exitosamente estudiantes a tu clase";
          if(vm.counter === vm.studentInvitation.length){
            $scope.modalInstanceSuccess = $uibModal.open({
              templateUrl: 'modules/modals/partials/modal-succefull.html',
              scope: $scope,
              size: 'sm',
              keyboard  : false
            });
          }
          vm.showSuccessNotification(vm.messageInvitedStudents);
        },function(error){
          vm.mensajeError = "¡Oops! Algo ha salido mal";
          $scope.modalInstanceError = $uibModal.open({
            templateUrl: 'modules/modals/partials/modal-error.html',
            scope: $scope,
            size: 'sm',
            keyboard  : false
          });
          vm.showErrorMessage();
        });
  };

  /**
  * @name showErrorMessage
  * @desc Shows error message to the user
  * @memberOf Controllers.StudentController
  */
  vm.showErrorMessage = function(){
    vm.showError = true;
    $timeout(function(){
      vm.showError = false;
    },2500);
  };


  vm.showSuccessNotification = function(message){
    vm.successMessage = message;
    vm.showSuccessMessage = true;
    $timeout(function(){
      vm.showSuccessMessage = false;
    },2500);
  };

  vm.studentMatterDesasociar = function(student){
    vm.student = student;
    $scope.modalInstance = $uibModal.open({
      templateUrl: 'modules/student/partials/modal/studentDisasociateMatter_modal.html',
      scope: $scope,
      size: 'md',
      keyboard  : false
    });
  };

  vm.removeStudent = function(){
    RemoveStudentToSection.delete({userId:vm.student.id,sectionId:vm.clase.section.id},null,
        function(success){
          var indexOfStudent = vm.clase.students.indexOf(vm.student);
          vm.clase.students.splice(indexOfStudent, 1);
          vm.clase.section.countStudent = vm.clase.section.countStudent-1;
          $scope.modalInstance.dismiss('cancel');
          vm.showSuccessNotification(vm.messageDessasociateStudent);
          vm.mensajeSuccessful = "¡Estudiante eliminado de la clase!";
          $scope.modalInstanceSuccess = $uibModal.open({
            templateUrl: 'modules/modals/partials/modal-succefull.html',
            scope: $scope,
            size: 'sm',
            keyboard  : false
          });
        },
        function(error){
          vm.mensajeError = "¡Oops! Algo ha salido mal";
          $scope.modalInstanceError = $uibModal.open({
            templateUrl: 'modules/modals/partials/modal-error.html',
            scope: $scope,
            size: 'sm',
            keyboard  : false
          });
          vm.showErrorMessage();
        });
  };

  vm.validationFecha = function(){
    var date = new Date();
    if(vm.student.date > date){
      vm.student.date = null;
      vm.messageDate = "Fecha de Nacimiento invalida";
      vm.showMessageDate = true;
      $timeout(function () {
        vm.showMessageDate = false;
      }, 2500);
    }
  };

    vm.validarCampos = function(){
      if(vm.student.identification == null || vm.student.identification == ""){
        vm.validarIdentification = true;
      }else{
        vm.validarIdentification = false;
        if(vm.student.firstName == null || vm.student.firstName == ""){
          vm.validationFirstName = true;
        }else{
          vm.validationFirstName = false;
          if(vm.student.secondName == null || vm.student.secondName == ""){
            vm.validationSecondName = true;
          }else{
            vm.validationSecondName = false;
            if(vm.student.lastName == null || vm.student.lastName == ""){
              vm.validationFirstLastName = true;
            }else{
              vm.validationFirstLastName = false;
              if(vm.student.secondLastName == null || vm.student.secondLastName == ""){
                vm.validationSecondLastName = true;
              }else{
                vm.validationSecondLastName = false;
                if(vm.student.date == null || vm.student.date == ""){
                  vm.validationDate = true;
                }else{
                  vm.validationDate = false;
                  if(vm.student.gender == null || vm.student.gender == ""){
                    vm.validationGender = true;
                  }else{
                    vm.validationGender = false;
                    if(vm.student.mail == null || vm.student.mail == ""){
                      vm.validationMail = true;
                    }else{
                      vm.validationMail = false;
                      if(vm.student.mailRepresentative == null || vm.student.mailRepresentative == ""){
                        vm.validationMailRepresentative = true;
                      }else{
                        vm.validationMailRepresentative = false;
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    };

    vm.cerrar = function(){
      $scope.modalInstanceSuccess.dismiss('cancel');
      if(vm.create){
        vm.create = false;
        $state.go('students');
      }
    };

    vm.salir = function(){
      $scope.modalInstanceError.dismiss('cancel');
    }

}
})();
