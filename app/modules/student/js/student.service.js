(function(){
    'use strict';

    angular
        .module('sckola.students')
        .factory('Students',students)
        .factory('Gender',gender)
        .factory('MatterTeacher',matterTeacher)
        .factory('Classes',classes)
        .factory('StudentsByCommunity',studentsByCommunity)
        .factory('StudentsByCommunityNotSection',studentsByCommunityNotSection)
        .factory('AddStudentToSection',addStudentToSection)
        .factory('RemoveStudentToSection',removeStudentToSection)
        .factory('StudentsBySection',studentsBySection)
        .factory('CreateStudentCommunty', createStudentCommunty);

    //ESTUDIANTES EXISTENTES EN UNA COMUNIDAD
    studentsByCommunity.$inject = ['$resource','$rootScope'];
    function studentsByCommunity($resource,$rootScope){
        return $resource($rootScope.domainServiceUrl+'/skola/user/role/:roleId/community/:communityId');
    };

    //Lista todos los estudiantes de la Seccion
    studentsBySection.$inject = ['$resource','$rootScope'];
    function studentsBySection($resource,$rootScope){
        return $resource($rootScope.domainServiceUrl+'/skola/user/section/:sectionId');
    };

    //ESTUDIANTES EXISTENTES EN UNA COMUNIDAD Y NO EN LA SECCION EXISTENTE
    studentsByCommunityNotSection.$inject = ['$resource','$rootScope'];
    function studentsByCommunityNotSection($resource,$rootScope){
        return $resource($rootScope.domainServiceUrl+'/skola/user/student/community/:communityId/not_section/:sectionId');
    };

    //ESTUDIANTES EXISTENTES EN UNA SECCION
    studentsBySection.$inject = ['$resource','$rootScope'];
    function studentsBySection($resource,$rootScope){
        return $resource($rootScope.domainServiceUrl+'/skola/user/section/:id');
    };

    //AGREGAR ESTUDIANTES A UNA SECCION
    addStudentToSection.$inject = ['$resource','$rootScope'];
    function addStudentToSection($resource,$rootScope){
        return $resource($rootScope.domainServiceUrl+'/skola/user/:userId/section/:sectionId',null,
            {
                save: {
                    method: 'POST'
                    /*headers: {
                     Authorization: 'Bearer '+ authentication.getToken()
                     }*/
                }
            });
    };

    //ELIMINAR ESTUDIANTE DE UNA SECCION
    removeStudentToSection.$inject = ['$resource','$rootScope'];
    function removeStudentToSection($resource,$rootScope){
        return $resource($rootScope.domainServiceUrl+'/skola/user/:userId/section/:sectionId',null,
            {
                delete: {
                    method: 'DELETE'
                    /*headers: {
                     Authorization: 'Bearer '+ authentication.getToken()
                     }*/
                }
            });
    };

    //Crear estudiantes en una comunidad
    createStudentCommunty.$inject = ['$resource','$rootScope'];
    function createStudentCommunty($resource,$rootScope){
        return $resource($rootScope.domainServiceUrl+'/skola/user/role/:roleId/community/:communityId',null,
            {
                save: {
                    method: 'POST'
                    /*headers: {
                     Authorization: 'Bearer '+ authentication.getToken()
                     }*/
                }
            });
    };

    students.$inject = ['$resource'];
    function students($resource){
        return $resource('static/data/students.json');
    };

    gender.$inject = ['$resource'];
    function gender($resource){
        return $resource('static/data/gender.json');
    };

    matterTeacher.$inject = ['$resource'];
    function matterTeacher($resource){
        return $resource('static/data/mattersTeacher.json');
    };

    classes.$inject = ['$resource'];
    function classes($resource){
        return $resource('static/data/classes.json');
    };
})();
